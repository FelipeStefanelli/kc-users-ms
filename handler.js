const { badRequest, success, internalServerError } = require('./src/utils/response');
const { parseEvent } = require('./src/utils/parser');
const { buildError } = require('./src/utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('./src/constants/errors');
const timeouts = require('./config/timeouts');
const i18n = require('./src/utils/i18n');

const { create } = require('./src/api/create');
const { update } = require('./src/api/update');
const { remove } = require('./src/api/remove');
const { get } = require('./src/api/get');
const { list } = require('./src/api/list');
const { listByContract } = require('./src/api/listByContract');
const { login } = require('./src/api/login');
const { listContracts } = require('./src/api/listContracts');
const { refresh } = require('./src/api/refresh');
const { authorizer } = require('./src/api/authorizer');

const abortTimeout = (promise, milliseconds) => {
  const timeout = new Promise((_, reject) =>
    setTimeout(() => reject(new Error(`Timeout limit reached (limit: ${milliseconds} ms)`)), milliseconds)
  );

  return Promise.race([timeout, promise]);
};

const handleAction = async (event, action) => {
  try {
    if (!event) {
      return badRequest(
        buildError(REQUEST_FIELD_REQUIRED('event', 'handleAction', 'handler', event), i18n.t('request.error'))
      );
    }

    if (!action) {
      return badRequest(
        buildError(
          REQUEST_FIELD_REQUIRED('action', 'handleAction', 'handler', action),
          i18n.t('request.error')
        )
      );
    }

    i18n.configure();

    if (event.headers && event.headers['Accept-Language']) {
      i18n.setLanguage(event.headers['Accept-Language']);
    }

    if (event.resource === '/login' || event.resource === '/refresh' || event.resource === '/authorizer') {
      const { data, errors } = await action(event);

      if (errors) {
        return badRequest({ errors });
      }

      return success({ data });
    } else {
      const parsedEvent = await parseEvent(event);

      if (parsedEvent.error) {
        return badRequest(buildError([{ details: parsedEvent.error }], i18n.t('request.error')));
      }

      const { data, errors } = await action(parsedEvent);

      if (errors) {
        return badRequest({ errors });
      }

      return success({ data });
    }
  } catch (error) {
    return internalServerError(buildError([{ details: error.message }], i18n.t('error')));
  }
};

const handler = async (event, action, timeoutTime) =>
  abortTimeout(handleAction(event, action), timeoutTime)
    .then((value) => {
      return value;
    })
    .catch((error) => {
      console.error(error);
      setTimeout(() => process.exit(), 1);
      return internalServerError(buildError([{ message: error.message }], i18n.t('error')));
    });

module.exports = {
  handleAction,
  handler,
  abortTimeout,
  create: async (event) => handler(event, create, timeouts.create),
  update: async (event) => handler(event, update, timeouts.update),
  remove: async (event) => handler(event, remove, timeouts.remove),
  get: async (event) => handler(event, get, timeouts.get),
  list: async (event) => handler(event, list, timeouts.list),
  listByContract: async (event) => handler(event, listByContract, timeouts.list),
  listContracts: async (event) => handler(event, listContracts, timeouts.list),
  login: async (event) => handler(event, login, timeouts.get),
  refresh: async (event) => handler(event, refresh, timeouts.get),
  authorizer: async (event) => handler(event, authorizer, timeouts.get),
};
