const { t } = require('../utils/i18n');

const buildUserSchema = (isUpdate) => {
  return {
    type: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.type.required'),
        stringEmpty: t('validation.fields.type.empty'),
      },
    },
    name: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.name.required'),
        stringEmpty: t('validation.fields.name.empty'),
      },
    },
    birth_date: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.birth_date.required'),
        stringEmpty: t('validation.fields.birth_date.empty'),
      },
    },
    identifier: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.identifier.required'),
        stringEmpty: t('validation.fields.identifier.empty'),
      },
    },
    identifier_type: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.identifier_type.required'),
        stringEmpty: t('validation.fields.identifier_type.empty'),
      },
    },
    password: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.password.required'),
        stringEmpty: t('validation.fields.password.empty'),
      },
    },
    contracts: {
      type: 'array',
      empty: false,
      nullable: false,
      optional: isUpdate,
      items: {
        type: 'string',
        empty: false,
        messages: {
          required: t('validation.fields.contracts.items.required'),
          stringEmpty: t('validation.fields.contracts.items.empty'),
        },
      },
      messages: {
        required: t('validation.fields.contracts.required'),
        arrayEmpty: t('validation.fields.contracts.empty'),
      },
    },
    address: {
      type: 'array',
      empty: false,
      optional: isUpdate,
      nullable: false,
      items: {
        type: 'object',
        props: {
          first_line: {
            type: 'string',
            messages: {
              required: t('validation.fields.address.first_line.required'),
              stringEmpty: t('validation.fields.address.first_line.empty'),
            },
          },
          complement: {
            type: 'string',
            messages: {
              required: t('validation.fields.address.complement.required'),
              stringEmpty: t('validation.fields.address.complement.empty'),
            },
          },
          city: {
            type: 'string',
            messages: {
              required: t('validation.fields.address.city.required'),
              stringEmpty: t('validation.fields.address.city.empty'),
            },
          },
          state: {
            type: 'string',
            messages: {
              required: t('validation.fields.address.state.required'),
              stringEmpty: t('validation.fields.address.state.empty'),
            },
          },
          country: {
            type: 'string',
            messages: {
              required: t('validation.fields.address.country.required'),
              stringEmpty: t('validation.fields.address.country.empty'),
            },
          },
          zip_code: {
            type: 'string',
            messages: {
              required: t('validation.fields.address.zip_code.required'),
              stringEmpty: t('validation.fields.address.zip_code.empty'),
            },
          },
        },
      },
      messages: {
        required: t('validation.fields.address.required'),
        arrayEmpty: t('validation.fields.address.empty'),
      },
    },
    address_verified: {
      type: 'boolean',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.address_verified.required')
      },
    },
    address_verification_photo: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.address_verification_photo.required'),
        stringEmpty: t('validation.fields.address_verification_photo.empty'),
      },
    },
    phone: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.phone.required'),
        stringEmpty: t('validation.fields.phone.empty'),
      },
    },
    phone_verified: {
      type: 'boolean',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.phone_verified.required')
      },
    },
    email: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.email.required'),
        stringEmpty: t('validation.fields.email.empty'),
      },
    },
    email_verified: {
      type: 'boolean',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.email_verified.required')
      },
    },
    first_login: {
      type: 'boolean',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.first_login.required'),
      },
    },
    welcome_email_sent: {
      type: 'boolean',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.welcome_email_sent.required')
      },
    },
    notifications: {
      type: 'array',
      empty: false,
      optional: isUpdate,
      nullable: false,
      items: {
        type: 'object',
        props: {
          reservations_alert: {
            type: 'boolean',
            messages: {
              required: t('validation.fields.notifications.reservations_alert.required')
            },
          },
          ride_alert: {
            type: 'boolean',
            messages: {
              required: t('validation.fields.notifications.ride_alert.required')
            },
          },
        },
      },
      messages: {
        required: t('validation.fields.notifications.required'),
        arrayEmpty: t('validation.fields.notifications.empty'),
      },
    },
    photo: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.photo.required'),
        stringEmpty: t('validation.fields.photo.empty'),
      },
    },
    driver_license_number: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.driver_license_number.required'),
        stringEmpty: t('validation.fields.driver_license_number.empty'),
      },
    },
    driver_license_register_number: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.driver_license_register_number.required'),
        stringEmpty: t('validation.fields.driver_license_register_number.empty'),
      },
    },
    driver_license_first_habilitation_at: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.driver_license_first_habilitation_at.required'),
        stringEmpty: t('validation.fields.driver_license_first_habilitation_at.empty'),
      },
    },
    driver_license_expires_at: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.driver_license_expires_at.required'),
        stringEmpty: t('validation.fields.driver_license_expires_at.empty'),
      },
    },
    driver_license_category: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.driver_license_category.required'),
        stringEmpty: t('validation.fields.driver_license_category.empty'),
      },
    },
    driver_license_photo: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.driver_license_photo.required'),
        stringEmpty: t('validation.fields.driver_license_photo.empty'),
      },
    },
    users_identifiers: {
      type: 'array',
      optional: true,
      items: {
        type: 'string'
      }
    },
    blocked: {
      type: 'boolean',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.blocked.required'),
      },
    },
    blocked_by: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.blocked_by.required'),
        stringEmpty: t('validation.fields.blocked_by.empty'),
      },
    },
    block_reason: {
      type: 'string',
      empty: false,
      optional: isUpdate,
      nullable: false,
      messages: {
        required: t('validation.fields.block_reason.required'),
        stringEmpty: t('validation.fields.block_reason.empty'),
      },
    }
  };
};

module.exports = {
  UserSchema: buildUserSchema,
};
