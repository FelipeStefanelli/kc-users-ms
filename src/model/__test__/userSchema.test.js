const { UserSchema } = require('../userSchema');
const { validUserSchema } = require('./helpers/userSchema.helper');

describe('schema test suit', () => {
  describe('when calling UserSchema function', () => {
    it('should return an UserSchema object', () => {
      const userSchema = UserSchema();

      expect(userSchema).toBeTruthy();
      expect(userSchema).toMatchObject(validUserSchema());
    });
  });

  describe.each([[false], [true]])('when passing isUpdate as %s to UserSchema', (isUpdate) => {
    it('should return an UserSchema object', () => {
      const userSchema = UserSchema(isUpdate);

      expect(userSchema).toBeTruthy();
      expect(userSchema).toStrictEqual(validUserSchema(isUpdate));
    });
  });
});
