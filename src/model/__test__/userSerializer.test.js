const { userSerializer } = require('../userSerializer');
const { t, configure } = require('../../utils/i18n');
const TestHelper = require('../../../config/tests/helper');

const validUser = { ...TestHelper.validUser.user };

describe('serializer test suit', () => {
  beforeAll(() => {
    configure();
  });

  describe('when passing a valid user', () => {
    it('should return an user', () => {
      const serializedUser = userSerializer(validUser);

      expect(serializedUser).toBeTruthy();
      expect(serializedUser).toMatchObject(validUser);
    });
  });

  describe.each([
    [null, 'The user field is required for model userSerializer method, received null'],
    [undefined, 'The user field is required for model userSerializer method, received undefined'],
  ])('when passing user as %s to userSerializer', (user, errorMessage) => {
    it('should return an error', () => {
      const serializedUser = userSerializer(user);

      expect(serializedUser.error).toBeTruthy();
      expect(serializedUser.error[0].details).toBe(errorMessage);
    });
  });
});
