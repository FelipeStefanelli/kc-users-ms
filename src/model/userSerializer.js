const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');

const userSerializer = (user) => {
  if (!user) {
    return { error: REQUEST_FIELD_REQUIRED('user', 'userSerializer', 'model', user) };
  }

  const {
    uuid,
    name,
    type,
    birth_date,
    identifier,
    identifier_type,
    password,
    contracts,
    address,
    address_verified,
    address_verification_photo,
    phone,
    phone_verified,
    email,
    email_verified,
    first_login,
    welcome_email_sent,
    notifications,
    photo,
    driver_license_number,
    driver_license_register_number,
    driver_license_first_habilitation_at,
    driver_license_expires_at,
    driver_license_category,
    driver_license_photo,
    users_identifiers,
    registered_at,
    activated_at,
    blocked,
    blocked_by,
    block_reason
  } = user;

  return {
    uuid,
    name,
    type,
    birth_date,
    identifier,
    identifier_type,
    password,
    contracts,
    address,
    address_verified,
    address_verification_photo,
    phone,
    phone_verified,
    email,
    email_verified,
    first_login,
    welcome_email_sent,
    notifications,
    photo,
    driver_license_number,
    driver_license_register_number,
    driver_license_first_habilitation_at,
    driver_license_expires_at,
    driver_license_category,
    driver_license_photo,
    users_identifiers,
    registered_at,
    activated_at,
    blocked,
    blocked_by,
    block_reason
  };
};

module.exports = {
  userSerializer,
};
