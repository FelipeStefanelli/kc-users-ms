(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global = global || self, global.FastestValidator = factory());
}(this, (function () { 'use strict';

	/**
	 * Flatten an array
	 * @param {Array} array
	 * @param {Array} target
	 * @returns Array flattened array
	 */
	function flatten(array, target) {
		var result = target || [];

		for (var i = 0; i < array.length; ++i) {
			if (Array.isArray(array[i])) {
				flatten(array[i], result);
			}
			else {
				result.push(array[i]);
			}
		}

		return result;
	}

	var flatten_1 = flatten;

	function deepExtend(destination, source) {
		for (var property in source) {
			if (typeof source[property] === "object" &&
				source[property] !== null) {
				destination[property] = destination[property] || {};
				deepExtend(destination[property], source[property]);
			} else {
				destination[property] = source[property];
			}
		}
		return destination;
	}

	var deepExtend_1 = deepExtend;

	var messages = {
		required: "The '{field}' field is required!",

		string: "The '{field}' field must be a string!",
		stringEmpty: "The '{field}' field must not be empty!",
		stringMin: "The '{field}' field length must be greater than or equal to {expected} characters long!",
		stringMax: "The '{field}' field length must be less than or equal to {expected} characters long!",
		stringLength: "The '{field}' field length must be {expected} characters long!",
		stringPattern: "The '{field}' field fails to match the required pattern!",
		stringContains: "The '{field}' field must contain the '{expected}' text!",
		stringEnum: "The '{field}' field does not match any of the allowed values!",
		stringNumeric: "The '{field}' field must be a numeric string",
		stringAlpha: "The '{field}' field must be an alphabetic string",
		stringAlphanum: "The '{field}' field must be an alphanumeric string",
		stringAlphadash: "The '{field}' field must be an alphadash string",

		number: "The '{field}' field must be a number!",
		numberMin: "The '{field}' field must be greater than or equal to {expected}!",
		numberMax: "The '{field}' field must be less than or equal to {expected}!",
		numberEqual: "The '{field}' field must be equal with {expected}!",
		numberNotEqual: "The '{field}' field can't be equal with {expected}!",
		numberInteger: "The '{field}' field must be an integer!",
		numberPositive: "The '{field}' field must be a positive number!",
		numberNegative: "The '{field}' field must be a negative number!",

		array: "The '{field}' field must be an array!",
		arrayEmpty: "The '{field}' field must not be an empty array!",
		arrayMin: "The '{field}' field must contain at least {expected} items!",
		arrayMax: "The '{field}' field must contain less than or equal to {expected} items!",
		arrayLength: "The '{field}' field must contain {expected} items!",
		arrayContains: "The '{field}' field must contain the '{expected}' item!",
		arrayEnum: "The '{field} field value '{expected}' does not match any of the allowed values!",

		boolean: "The '{field}' field must be a boolean!",

		function: "The '{field}' field must be a function!",

		date: "The '{field}' field must be a Date!",
		dateMin: "The '{field}' field must be greater than or equal to {expected}!",
		dateMax: "The '{field}' field must be less than or equal to {expected}!",

		forbidden: "The '{field}' field is forbidden!",

		email: "The '{field}' field must be a valid e-mail!",

		url: "The '{field}' field must be a valid URL!",

		enumValue: "The '{field} field value '{expected}' does not match any of the allowed values!",

		object: "The '{field}' must be an Object!",
		objectStrict: "The object '{field}' contains invalid keys: '{actual}'!",
		uuid: "The {field} field must be a valid UUID",
		uuidVersion: "The {field} field must be a valid version provided",
		mac: "The {field} field must be a valid MAC address",
		luhn: "The {field} field must be a valid checksum luhn",
	};

	var any = function checkAny() {
		return true;
	};

	var array = function checkArray(value, schema) {
		if (!Array.isArray(value)) {
			return this.makeError("array");
		}

		var arrLength = value.length;

		if (schema.empty === false && arrLength === 0) {
			return this.makeError("arrayEmpty");
		}

		if (schema.min != null && arrLength < schema.min) {
			return this.makeError("arrayMin", schema.min, arrLength);
		}

		if (schema.max != null && arrLength > schema.max) {
			return this.makeError("arrayMax", schema.max, arrLength);
		}

		// Check fix length
		if (schema.length != null && arrLength !== schema.length) {
			return this.makeError("arrayLength", schema.length, arrLength);
		}	

		if (schema.contains != null && value.indexOf(schema.contains) === -1) {
			return this.makeError("arrayContains", schema.contains);
		}	

		if (schema.enum != null) {
			for (var i = 0; i < value.length; i++) {
				if (schema.enum.indexOf(value[i]) === -1) {
					return this.makeError("arrayEnum", value[i], schema.enum);
				}
			}
		}	

		return true;
	};

	var boolean_1 = function checkBoolean(value, schema) {
		if (schema.convert === true && typeof value !== "boolean") {
			if (
				value === 1
			|| value === 0
			|| value === "true"
			|| value === "false"
			|| value === "1"
			|| value === "0"
			|| value === "on"
			|| value === "off"
			) 
				{ return true; }
		}
		
		if (typeof value !== "boolean") {
			return this.makeError("boolean");
		}

		return true;
	};

	var custom = function customCheck(value, schema) {
		return schema.check.call(this, value, schema);
	};

	var date = function checkDate(value, schema) {
		if (schema.convert === true && !(value instanceof Date)) {
			value = new Date(value);
		}
		
		if (!(value instanceof Date)) {
			return this.makeError("date");
		}

		if (isNaN(value.getTime())) {
			return this.makeError("date");
		}

		return true;
	};

	var PRECISE_PATTERN = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var BASIC_PATTERN = /^\S+@\S+\.\S+$/;

	var email = function checkEmail(value, schema) {
		if (typeof value !== "string") {
			return this.makeError("string");
		}

		var pattern;
		if (schema.mode == "precise")
			{ pattern = PRECISE_PATTERN; }
		else
			{ pattern = BASIC_PATTERN; }

		if (!pattern.test(value)) {
			return this.makeError("email");
		}

		return true;
	};

	var _enum = function checkEnum(value, schema) {

		if (schema.values != null && schema.values.indexOf(value) === -1) {
			return this.makeError("enumValue", schema.values, value);
		}

		return true;
	};

	var forbidden = function checkForbidden(value) {
		if (value !== null && value !== undefined) {
			return this.makeError("forbidden");
		}

		return true;
	};

	var _function = function checkFunction(value) {
		if (typeof value !== "function") {
			return this.makeError("function");
		}

		return true;
	};

	var number = function checkNumber(value, schema) {
		if (schema.convert === true && typeof value !== "number") {
			value = Number(value);
		}

		if (typeof value !== "number") {
			return this.makeError("number");
		}

		if (isNaN(value) || !isFinite(value)) {
			return this.makeError("number");
		}

		if (schema.min != null && value < schema.min) {
			return this.makeError("numberMin", schema.min, value);
		}

		if (schema.max != null && value > schema.max) {
			return this.makeError("numberMax", schema.max, value);
		}

		// Check fix value
		if (schema.equal != null && value !== schema.equal) {
			return this.makeError("numberEqual", schema.equal, value);
		}	

		// Check not fix value
		if (schema.notEqual != null && value === schema.notEqual) {
			return this.makeError("numberNotEqual", schema.notEqual);
		}	

		// Check integer
		if (schema.integer === true && value % 1 !== 0) {
			return this.makeError("numberInteger", value);
		}	

		// Check positive
		if (schema.positive === true && value <= 0) {
			return this.makeError("numberPositive", value);
		}	

		// Check negative
		if (schema.negative === true && value >= 0) {
			return this.makeError("numberNegative", value);
		}

		return true;
	};

	var object = function checkObject(value, schema) {
		if (typeof value !== "object" || value === null || Array.isArray(value)) {
			return this.makeError("object");
		}

		if (schema.strict === true && schema.props) {
			var allowedProps = Object.keys(schema.props);
			var invalidProps = [];
			var props = Object.keys(value);

			for (var i = 0; i < props.length; i++) {
				if (allowedProps.indexOf(props[i]) === -1) {
					invalidProps.push(props[i]);
				}
			}
			if (invalidProps.length !== 0) {
				return this.makeError("objectStrict", undefined, invalidProps.join(", "));
			}
		}

		return true;
	};

	var NUMERIC_PATTERN = /^-?[0-9]\d*(\.\d+)?$/;
	var ALPHA_PATTERN = /^[a-zA-Z]+$/;
	var ALPHANUM_PATTERN = /^[a-zA-Z0-9]+$/;
	var ALPHADASH_PATTERN = /^[a-zA-Z0-9_-]+$/;

	var string = function checkString(value, schema) {
		if (typeof value !== "string") {
			return this.makeError("string");
		}

		var valueLength = value.length;

		if (schema.empty === false && valueLength === 0) {
			return this.makeError("stringEmpty");
		}

		if (schema.min != null && valueLength < schema.min) {
			return this.makeError("stringMin", schema.min, valueLength);
		}

		if (schema.max != null && valueLength > schema.max) {
			return this.makeError("stringMax", schema.max, valueLength);
		}

		if (schema.length != null && valueLength !== schema.length) {
			return this.makeError("stringLength", schema.length, valueLength);
		}

		if (schema.pattern != null) {
			var pattern = typeof schema.pattern == "string" ? new RegExp(schema.pattern, schema.patternFlags) : schema.pattern;
			if (!pattern.test(value))
				{ return this.makeError("stringPattern", pattern, value); }
		}

		if (schema.contains != null && value.indexOf(schema.contains) === -1) {
			return this.makeError("stringContains", schema.contains, value);
		}

		if (schema.enum != null && schema.enum.indexOf(value) === -1) {
			return this.makeError("stringEnum", schema.enum, value);
		}

		if (schema.numeric === true && !NUMERIC_PATTERN.test(value) ) {
			return this.makeError("stringNumeric", "A numeric string", value);
		}

		if(schema.alpha === true && !ALPHA_PATTERN.test(value)) {
			return this.makeError("stringAlpha", "An alphabetic string", value);
		}

		if(schema.alphanum === true && !ALPHANUM_PATTERN.test(value)) {
			return this.makeError("stringAlphanum", "An alphanumeric string", value);
		}

		if(schema.alphadash === true && !ALPHADASH_PATTERN.test(value)) {
			return this.makeError("stringAlphadash", "An alphadash string", value);
		}

		return true;
	};

	var PATTERN = /^https?:\/\/\S+/;
	//const PATTERN = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
	//const PATTERN = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;

	var url = function checkUrl(value) {
		if (typeof value !== "string") {
			return this.makeError("string");
		}
		
		if (!PATTERN.test(value)) {
			return this.makeError("url");
		}

		return true;
	};

	var PATTERN$1 = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[0-9a-f]{4}-[0-9a-f]{12}$/i;

	var uuid = function checkUUID(value, schema) {
		if (typeof value !== "string")
			{ return this.makeError("string"); }

		value = value.toLowerCase();
		if (!PATTERN$1.test(value))
			{ return this.makeError("uuid"); }

		var version = value.charAt(14)|0;
		if(schema.version && schema.version !== version)
			{ return this.makeError("uuidVersion", schema.version, version); }

		switch (version) {
		case 1:
		case 2:
			return true;
		case 3:
		case 4:
		case 5:
			return ["8", "9", "a", "b"].indexOf(value.charAt(19)) !== -1 || this.makeError("uuid");
		}
	};

	var PATTERN$2 = /^((([a-f0-9][a-f0-9]+[-]){5}|([a-f0-9][a-f0-9]+[:]){5})([a-f0-9][a-f0-9])$)|(^([a-f0-9][a-f0-9][a-f0-9][a-f0-9]+[.]){2}([a-f0-9][a-f0-9][a-f0-9][a-f0-9]))$/i;

	var mac = function checkMAC(value, schema) {
		if (typeof value !== "string")
			{ return this.makeError("string"); }

		value = value.toLowerCase();
		if (!PATTERN$2.test(value))
			{ return this.makeError("mac"); }

		return true;
	};

	/**
	 * Luhn algorithm checksum https://en.wikipedia.org/wiki/Luhn_algorithm
	 * Credit Card numbers, IMEI numbers, National Provider Identifier numbers and others
	 * @param value
	 * @param schema
	 * @return {boolean|{actual, expected, type}|ValidationError}
	 */
	var luhn = function checkLuhn(value, schema) {

		if(typeof value !== "number" && typeof value !== "string")
			{ return this.makeError("string"); }

		if (typeof value !== "string")
			{ value = String(value); }

		value = value.replace(/\D+/g, "");

		var check = function (array) {
			return function (number) {
				var len = number ? number.length : 0,
					bit = 1,
					sum = 0;
				while (len--) {
					sum += !(bit ^= 1) ? parseInt(number[len], 10) : array[number[len]];
				}
				return sum % 10 === 0 && sum > 0;
			};
		}([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]);

		return check(value) || this.makeError("luhn");
	};

	function loadRules() {
		return {
			any: any,
			array: array,
			boolean: boolean_1,
			custom: custom,
			date: date,
			email: email,
			enum: _enum,
			forbidden: forbidden,
			function: _function,
			number: number,
			object: object,
			string: string,
			url: url,
			uuid: uuid,
			mac: mac,
			luhn: luhn
		};
	}

	// Quick regex to match most common unquoted JavaScript property names. Note the spec allows Unicode letters.
	// Unmatched property names will be quoted and validate slighly slower. https://www.ecma-international.org/ecma-262/5.1/#sec-7.6
	var identifierRegex = /^[_$a-zA-Z][_$a-zA-Z0-9]*$/;

	// Regex to escape quoted property names for eval/new Function
	var escapeEvalRegex = /["'\\\n\r\u2028\u2029]/g;

	function escapeEvalString(str) {
		// Based on https://github.com/joliss/js-string-escape
		return str.replace(escapeEvalRegex, function(character) {
			switch (character) {
			case "\"":
			case "'":
			case "\\":
				return "\\" + character;
				// Four possible LineTerminator characters need to be escaped:
			case "\n":
				return "\\n";
			case "\r":
				return "\\r";
			case "\u2028":
				return "\\u2028";
			case "\u2029":
				return "\\u2029";
			}
		});
	}

	/**
	 * Validator class constructor
	 *
	 * @param {Object} opts
	 */
	function Validator(opts) {
		this.opts = {
			messages: deepExtend_1({}, messages)
		};

		if (opts)
			{ deepExtend_1(this.opts, opts); }

		this.messages = this.opts.messages;
		this.messageKeys = Object.keys(this.messages);

		// Load rules
		this.rules = loadRules();
		this.cache = new Map();
	}

	/**
	 * Validate an object by schema
	 *
	 * @param {Object} obj
	 * @param {Object} schema
	 */
	Validator.prototype.validate = function(obj, schema) {
		var check = this.compile(schema);
		return check(obj);
	};

	/**
	 * Compile a schema
	 *
	 * @param {Object} schema
	 * @throws {Error} Invalid schema
	 */
	Validator.prototype.compile = function(schema) {
		var self = this;
		if (Array.isArray(schema)) {
		// Multiple schemas
			if (schema.length == 0) {
				throw new Error("If the schema is an Array, must contain at least one element!");
			}

			var rules = this.compileSchemaType(schema);
			this.cache.clear();
			return function(value, path, parent) {
				return self.checkSchemaType(value, rules, path, parent || null);
			};
		}

		var rule = this.compileSchemaObject(schema);
		this.cache.clear();
		return function(value, path, parent) {
			return self.checkSchemaObject(value, rule, path, parent || null);
		};
	};

	Validator.prototype.compileSchemaObject = function(schemaObject) {
		var this$1 = this;

		if (schemaObject === null || typeof schemaObject !== "object" || Array.isArray(schemaObject)) {
			throw new Error("Invalid schema!");
		}

		var compiledObject = this.cache.get(schemaObject);
		if (compiledObject) {
			compiledObject.cycle = true;
			return compiledObject;
		} else {
			compiledObject = { cycle: false, properties: null, compiledObjectFunction: null, objectStack: [] };
			this.cache.set(schemaObject, compiledObject);
		}

		compiledObject.properties = Object.keys(schemaObject)
			.filter(function (name) {
				return name !== "$$strict";
			})
			.map(function (name) {
				var compiledType = this$1.compileSchemaType(schemaObject[name]);
				return { name: name, compiledType: compiledType };
			});

		var sourceCode = [];
		sourceCode.push("let res;");
		sourceCode.push("let propertyPath;");
		sourceCode.push("const errors = [];");

		if (schemaObject.$$strict === true) {
			sourceCode.push("const givenProps = new Map(Object.keys(value).map(key => [key, true]));");
		}

		for (var i = 0; i < compiledObject.properties.length; i++) {
			var property = compiledObject.properties[i];
			var name = escapeEvalString(property.name);
			var propertyValueExpr = identifierRegex.test(name) ? ("value." + name) : ("value[\"" + name + "\"]");

			sourceCode.push(("propertyPath = (path !== undefined ? path + \"." + name + "\" : \"" + name + "\");"));
			if (Array.isArray(property.compiledType)) {
				sourceCode.push(("res = this.checkSchemaType(" + propertyValueExpr + ", properties[" + i + "].compiledType, propertyPath, value);"));
			} else {
				sourceCode.push(("res = this.checkSchemaRule(" + propertyValueExpr + ", properties[" + i + "].compiledType, propertyPath, value);"));
			}
			sourceCode.push("if (res !== true) {");
			sourceCode.push(("\tthis.handleResult(errors, propertyPath, res, properties[" + i + "].compiledType.messages);"));
			sourceCode.push("}");

			if (schemaObject.$$strict === true) {
				sourceCode.push(("givenProps.delete(\"" + name + "\");"));
			}
		}

		if (schemaObject.$$strict === true) {
			sourceCode.push("if (givenProps.size !== 0) {");
			sourceCode.push("\tthis.handleResult(errors, path || 'rootObject', this.makeError('objectStrict', undefined, [...givenProps.keys()].join(', ')), this.messages);");
			sourceCode.push("}");
		}

		sourceCode.push("return errors.length === 0 ? true : errors;");

		compiledObject.compiledObjectFunction = new Function("value", "properties", "path", "parent", sourceCode.join("\n"));

		return compiledObject;
	};

	Validator.prototype.compileSchemaType = function(schemaType) {
		var this$1 = this;


		if (Array.isArray(schemaType)) {

			// Multiple rules, flatten to array of compiled SchemaRule
			var rules = flatten_1(schemaType.map(function (r) { return this$1.compileSchemaType(r); }));

			if (rules.length == 1) {
				return rules[0];
			}

			return rules;
		}

		return this.compileSchemaRule(schemaType);

	};

	Validator.prototype.compileMessages = function(schemaType) {
		if (schemaType.messages)
			{ return Object.assign({}, this.messages, schemaType.messages); }

		return this.messages;
	};

	Validator.prototype.compileSchemaRule = function(schemaRule) {

		if (typeof schemaRule === "string") {
			schemaRule = {
				type: schemaRule
			};
		}

		var ruleFunction = this.rules[schemaRule.type];

		if (!ruleFunction) {
			throw new Error("Invalid '" + schemaRule.type + "' type in validator schema!");
		}

		var messages = this.compileMessages(schemaRule);

		var dataParameter = null;
		var dataFunction = null;

		if (schemaRule.type === "object" && schemaRule.props) {
			dataParameter = this.compileSchemaObject(schemaRule.props);
			dataFunction = this.checkSchemaObject;
		} else if (schemaRule.type === "array" && schemaRule.items) {
			dataParameter = this.compileSchemaType(schemaRule.items);
			dataFunction = this.checkSchemaArray;
		}

		return {
			messages: messages,
			schemaRule: schemaRule,
			ruleFunction: ruleFunction,
			dataFunction: dataFunction,
			dataParameter: dataParameter
		};
	};

	Validator.prototype.checkSchemaObject = function(value, compiledObject, path, parent) {
		if (compiledObject.cycle) {
			if (compiledObject.objectStack.indexOf(value) !== -1) {
				return true;
			}

			compiledObject.objectStack.push(value);
			var result = this.checkSchemaObjectInner(value, compiledObject, path, parent);
			compiledObject.objectStack.pop();
			return result;
		} else {
			return this.checkSchemaObjectInner(value, compiledObject, path, parent);
		}
	};

	Validator.prototype.checkSchemaObjectInner = function(value, compiledObject, path, parent) {
		return compiledObject.compiledObjectFunction.call(this, value, compiledObject.properties, path, parent);

		/*
	    // Reference implementation of the object checker

	    const errors = [];
	    const propertiesLength = compiledObject.properties.length;
	    for (let i = 0; i < propertiesLength; i++) {
	    const property = compiledObject.properties[i];
	    const propertyPath = (path !== undefined ? path + "." : "") + property.name;
	    const res = this.checkSchemaType(value[property.name], property.compiledType, propertyPath, value);

	    if (res !== true) {
	    this.handleResult(errors, propertyPath, res);
	    }
	    }

	    return errors.length === 0 ? true : errors;
	    */
	};

	Validator.prototype.checkSchemaType = function(value, compiledType, path, parent) {

		if (Array.isArray(compiledType)) {

			var errors = [];
			var checksLength = compiledType.length;
			for (var i = 0; i < checksLength; i++) {
				// Always compiled to list of rules
				var res = this.checkSchemaRule(value, compiledType[i], path, parent);

				if (res !== true) {
					this.handleResult(errors, path, res, compiledType.messages);
				} else {
					// Jump out after first success and clear previous errors
					return true;
				}
			}

			return errors;
		}

		return this.checkSchemaRule(value, compiledType, path, parent);
	};

	Validator.prototype.checkSchemaArray = function(value, compiledArray, path, parent) {
		var errors = [];
		var valueLength = value.length;

		for (var i = 0; i < valueLength; i++) {
			var itemPath = (path !== undefined ? path : "") + "[" + i + "]";
			var res = this.checkSchemaType(value[i], compiledArray, itemPath, value, parent);

			if (res !== true) {
				this.handleResult(errors, itemPath, res, compiledArray.messages);
			}
		}

		return errors.length === 0 ? true : errors;
	};

	Validator.prototype.checkSchemaRule = function(value, compiledRule, path, parent) {
		var schemaRule = compiledRule.schemaRule;

		if (value === undefined) {
			if (schemaRule.type === "forbidden")
				{ return true; }

			if (schemaRule.optional === true && !parent.hasOwnProperty(path)) { return true; }

			var errors = [];
			this.handleResult(errors, path, this.makeError("required"), compiledRule.messages);

			return errors;
		}

		if (value === null) {
			if (schemaRule.nullable === true)
				{ return true; }

			var errors = [];
			this.handleResult(errors, path, this.makeError("required"), compiledRule.messages);

			return errors;
		}

		var res = compiledRule.ruleFunction.call(this, value, schemaRule, path, parent);

		if (res !== true) {
			var errors$1 = [];
			this.handleResult(errors$1, path, res, compiledRule.messages);

			return errors$1;
		}

		if (compiledRule.dataFunction !== null) {
			return compiledRule.dataFunction.call(this, value, compiledRule.dataParameter, path, parent);
		}

		return true;
	};

	/**
	 * Handle results from validator functions
	 *
	 * @param {Array} errors
	 * @param {String} fieldPath
	 * @param {Array|Object} res
	 */
	Validator.prototype.handleResult = function(errors, fieldPath, res, messages) {
		var this$1 = this;

		var items;
		if (!Array.isArray(res))
			{ items = [res]; }
		else
			{ items = res; }

		items.forEach(function (err) {
			if (!err.field)
				{ err.field = fieldPath; }
			if (!err.message)
				{ err.message = this$1.resolveMessage(err, messages[err.type]); }

			errors.push(err);
		});
	};

	/**
	 * Create a validation error object
	 *
	 * @param {String} type
	 * @param {Any} expected
	 * @param {Any} actual
	 */
	Validator.prototype.makeError = function(type, expected, actual) {
		return {
			type: type,
			expected: expected,
			actual: actual
		};
	};

	/**
	 * Resolve message string from a validation error object
	 *
	 * @param {Object} err Validation error object
	 */
	Validator.prototype.resolveMessage = function(err, msg) {
		if ( msg === void 0 ) msg = null;


		if (msg != null) {
			var expected = err.expected != null ? err.expected : "";
			var actual = err.actual != null ? err.actual : "";
			return msg.replace(/\{field\}/g, err.field).replace(/\{expected\}/g, expected).replace(/\{actual\}/g, actual);
		}
	};

	/**
	 * Add a custom validator rule
	 *
	 * @param {String} type
	 * @param {Function} fn
	 */
	Validator.prototype.add = function(type, fn) {
		this.rules[type] = fn;
	};

	var validator = Validator;

	var fastestValidator = validator;

	return fastestValidator;

})));
//# sourceMappingURL=index.js.map
