const Validator = require('../lib/fastest-validator');
const { UserSchema } = require('../model/userSchema');

const validateSchema = async (obj, schema) => {
  const validator = new Validator();

  const validationResult = validator.validate(obj, schema);
  const hasValidationErrors = validationResult !== true;

  const response = {
    isValid: !hasValidationErrors,
    errors: hasValidationErrors && validationResult,
  };

  return response;
};

const validateUser = async (user, isUpdate) => {
  return validateSchema(user, UserSchema(isUpdate));
};

module.exports = {
  validateUser,
};
