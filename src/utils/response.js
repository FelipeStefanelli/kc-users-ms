const httpStatusCodes = {
  OK: 200,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  INTERNAL_SERVER_ERROR: 500,
};

const createResponse = (statusCode, body) => {
  if (!statusCode) {
    return { error: [{ details: 'The statusCode field is required for utils createResponse method' }] };
  }

  if (typeof statusCode !== 'number') {
    return { error: [{ details: 'The statusCode field must be a number for utils createResponse method' }] };
  }

  if (!body) {
    return { error: [{ details: 'The body field is required for utils createResponse method' }] };
  }

  if (typeof body !== 'object') {
    return { error: [{ details: 'The body field must be an object for utils createResponse method' }] };
  }

  return {
    statusCode,
    body: JSON.stringify(body),
  };
};

const badRequest = (body) => {
  return createResponse(httpStatusCodes.BAD_REQUEST, body);
};

const success = (body) => {
  return createResponse(httpStatusCodes.OK, body);
};

const internalServerError = (body) => {
  return createResponse(httpStatusCodes.INTERNAL_SERVER_ERROR, body);
};

module.exports = {
  createResponse,
  badRequest,
  success,
  internalServerError,
  httpStatusCodes,
};
