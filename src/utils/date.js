const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');

const addMinutes = (dateTime, minutes) => {
  if (!dateTime) {
    return { error: REQUEST_FIELD_REQUIRED('dateTime', 'addMinutes', 'utils', dateTime) };
  }

  if (!minutes) {
    return { error: REQUEST_FIELD_REQUIRED('minutes', 'addMinutes', 'utils', minutes) };
  }

  if (minutes < 0) {
    return {
      error: [
        {
          field: 'minutes',
          details: `The minutes field must be greater than 0 on utils addMinutes method, received ${minutes}`,
        },
      ],
    };
  }

  const newDateTime = new Date(dateTime);
  const dateTimeMinutes = newDateTime.getMinutes();

  newDateTime.setMinutes(dateTimeMinutes + minutes);

  return newDateTime;
};

module.exports = { addMinutes };
