const {
  URL_QUERY_FILTER_PARAMS_SEPARATOR,
  URL_KEY_CONDITIONS_PARAMS_SEPARATOR,
} = require('../constants/urlSeparators');

const parseJSON = (stringifiedJson) => {
  if (!stringifiedJson) {
    return { error: [{ details: 'The stringifiedJson field is required for utils parseJSON method' }] };
  }

  try {
    const parsedJson = JSON.parse(stringifiedJson);
    return parsedJson;
  } catch (error) {
    return { error: [{ details: error.message }] };
  }
};

/**
 * @returns {body, queryString} body and queryString can be null
 */

const parseEvent = (event) => {
  if (!event) {
    return { error: [{ details: 'The event field is required for utils parseEvent method' }] };
  }

  const { body, queryStringParameters, pathParameters } = event;
  let parsedBody;
  let queryString;

  if (body) {
    parsedBody = parseJSON(body);
  }

  if (parsedBody && parsedBody.error) {
    return { error: parsedBody.error };
  }

  if (queryStringParameters) {
    queryString = queryStringParameters;
  }


  const response = {
    body: parsedBody || {},
    queryString: queryString || {},
    pathParameters: pathParameters || {}
  };

  return response;
};

const buildUpdateParams = (params) => {
  const updateParams = {};

  Object.keys(params).forEach((paramKey) => {
    const value = params[paramKey];

    updateParams[paramKey] = {
      Action: 'PUT',
      Value: value,
    };
  });

  return updateParams;
};

const buildParams = (params, urlSeparator) => {
  const object = {};
  const errors = [];

  Object.keys(params).forEach((paramKey) => {
    let value = params[paramKey];
    const splittedExpression = paramKey.split(urlSeparator);

    const attributeParam = splittedExpression[0];

    let error;

    if (error) {
      errors.push(error);
    }

    const comparisonOperatorParam = splittedExpression[1];

    object[attributeParam] = {
      ComparisonOperator: comparisonOperatorParam,
      AttributeValueList: [value],
    };
  });

  if (errors.length > 0) {
    return { errors };
  }

  return object;
};

const parseParams = (params = {}) => {
  if (!params) {
    params = {};
  }

  const paramsKeys = Object.keys(params);

  const filterParamsKeys = paramsKeys.filter((param) => param.includes(URL_QUERY_FILTER_PARAMS_SEPARATOR));

  const keyConditionParamsKeys = paramsKeys.filter((param) =>
    param.includes(URL_KEY_CONDITIONS_PARAMS_SEPARATOR)
  );

  const queryParamsKeys = paramsKeys.filter(
    (param) =>
      !param.includes(URL_QUERY_FILTER_PARAMS_SEPARATOR) &&
      !param.includes(URL_KEY_CONDITIONS_PARAMS_SEPARATOR)
  );

  let filterParams = {};
  let keyConditionParams = {};
  const queryParams = {};

  filterParamsKeys.forEach((paramKey) => {
    filterParams[paramKey] = params[paramKey];
  });

  keyConditionParamsKeys.forEach((paramKey) => {
    keyConditionParams[paramKey] = params[paramKey];
  });

  queryParamsKeys.forEach((paramKey) => {
    queryParams[paramKey] = params[paramKey];
  });

  filterParams = buildParams(filterParams, URL_QUERY_FILTER_PARAMS_SEPARATOR);
  keyConditionParams = buildParams(keyConditionParams, URL_KEY_CONDITIONS_PARAMS_SEPARATOR);

  if (filterParams.errors) {
    return { error: filterParams.errors };
  }

  if (keyConditionParams.errors) {
    return { error: keyConditionParams.errors };
  }

  return {
    filterParams,
    keyConditionParams,
    queryParams,
  };
};

module.exports = {
  parseEvent,
  parseJSON,
  parseParams,
  buildUpdateParams,
  buildParams,
};
