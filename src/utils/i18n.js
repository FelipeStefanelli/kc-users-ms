const i18n = require('i18n');
const acceptLanguageParser = require('accept-language-parser');
const path = require('path');

let __configured = false;

const configure = () => {
  i18n.configure({
    locales: ['en', 'pt'],
    defaultLocale: 'en',
    directory: path.join(__dirname, '../../config/locales'),
    objectNotation: true,
    updateFiles: false,
  });
  __configured = true;
};

const setLanguage = (language) => {
  if (!language || typeof language !== 'string') {
    return;
  }

  const languages = acceptLanguageParser.parse(language);
  const languageCode = languages[0].code;

  i18n.setLocale(languageCode);
};

const t = (...args) => {
  if (!__configured) {
    configure();
  }

  return i18n.__(...args);
};

module.exports = {
  setLanguage,
  configure,
  t,
  i18n,
};
