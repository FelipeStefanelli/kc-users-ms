const { buildError } = require('../errors');

const {
  errorsList,
  errorsTitle,
  validError,
  validErrorWithDefaultMessage,
  validErrorWithNullErrorsList,
  validErrorWithNullErrorsListAndDefaultMessage,
} = require('./helpers/errors.helper');

const { t } = require('../i18n');

describe('buildError test suit', () => {
  describe.each([
    [errorsList, errorsTitle, validError],
    [null, errorsTitle, validErrorWithNullErrorsList],
    [undefined, errorsTitle, validErrorWithNullErrorsList],
    [errorsList, null, validErrorWithDefaultMessage],
    [errorsList, undefined, validErrorWithDefaultMessage],
    [null, null, validErrorWithNullErrorsListAndDefaultMessage],
    [undefined, undefined, validErrorWithNullErrorsListAndDefaultMessage],
  ])(
    'when passing errorList as %j and errorTitle as %j to buildError',
    (errorList, errorTitle, errorObject) => {
      it('should return a valid error', () => {
        const error = buildError(errorList, errorTitle);

        expect(error).toBeTruthy();
        expect(error).toMatchObject(errorObject);
        expect(error.errors).toHaveProperty('title');
        expect(error.errors.title).toBeTruthy();
        expect(error.errors.title).toEqual(t('request.error'));
      });
    }
  );
});
