const validParams = {
  limit: 10,
  last_evaluated_key: 'abc',
  'contract@EQ': 'contract_16b50910-5cad-5f41-a46d-f1a8462f2b00',
};

const validParsedKeyConditionsParams = {
  contract: {
    ComparisonOperator: 'EQ',
    AttributeValueList: ['contract_16b50910-5cad-5f41-a46d-f1a8462f2b00'],
  },
};

const validParsedQueryFilterParams = {
  range: {
    ComparisonOperator: 'GT',
    AttributeValueList: [4],
  },
};

const validUpdateParams = {
  name: "pipoFOI",
  contracts: [
      "19283746-5cad-5f41-a46d-f1a8462f2b00"
  ]
};

const validParsedUpdateParams = {
  name: {
    Action: 'PUT',
    Value: 'pipoFOI',
  },
  contracts: {
    Action: 'PUT',
    Value: [
      "19283746-5cad-5f41-a46d-f1a8462f2b00"
  ],
  },
};

const validParsedQueryParams = {
  limit: 10,
  last_evaluated_key: 'abc',
};

const validKeyConditionsParams = { 'contract@EQ': 'contract_16b50910-5cad-5f41-a46d-f1a8462f2b00' };
const validFilterParams = { range$GT: 4 };
const validQueryParams = { limit: 10, last_evaluated_key: 'abc' };

module.exports = {
  validParams,
  validParsedKeyConditionsParams,
  validParsedQueryFilterParams,
  validKeyConditionsParams,
  validFilterParams,
  validQueryParams,
  validUpdateParams,
  validParsedUpdateParams,
  validParsedQueryParams,
};
