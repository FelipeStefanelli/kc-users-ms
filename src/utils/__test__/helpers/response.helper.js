const validBody = {
  data: { user: 'User' },
};

const validStatusCode = 200;

module.exports = {
  validBody,
  validStatusCode,
};
