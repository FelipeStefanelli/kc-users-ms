const { t } = require('../../i18n');

const validError = {
  errors: {
    validation: [
      {
        type: 'stringEmpty',
        field: 'uuid',
        message: 'Uuid must not be empty',
      },
      {
        type: 'stringEmpty',
        field: 'type',
        message: 'Type must not be empty',
      },
    ],
    title: 'The request has failed, please try again',
  },
};

const validErrorWithDefaultMessage = {
  errors: {
    validation: [
      {
        type: 'stringEmpty',
        field: 'uuid',
        message: 'Uuid must not be empty',
      },
      {
        type: 'stringEmpty',
        field: 'type',
        message: 'Type must not be empty',
      },
    ],
    title: t('request.error'),
  },
};

const validErrorWithNullErrorsList = {
  errors: {
    title: 'The request has failed, please try again',
  },
};

const validErrorWithNullErrorsListAndDefaultMessage = {
  errors: {
    title: t('request.error'),
  },
};

const errorsList = [
  {
    type: 'stringEmpty',
    field: 'uuid',
    message: 'Uuid must not be empty',
  },
  {
    type: 'stringEmpty',
    field: 'type',
    message: 'Type must not be empty',
  },
];

const errorsTitle = 'The request has failed, please try again';

module.exports = {
  validError,
  validErrorWithDefaultMessage,
  validErrorWithNullErrorsList,
  validErrorWithNullErrorsListAndDefaultMessage,
  errorsList,
  errorsTitle,
};
