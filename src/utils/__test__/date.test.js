const { addMinutes } = require('../date');

const dateString = '2019-12-12T10:00:00Z';

describe('addMinutes test suit', () => {
  describe.each([
    [new Date(dateString), 6, '2019-12-12T10:06:00.000Z'],
    [new Date(dateString), 60, '2019-12-12T11:00:00.000Z'],
    [new Date(dateString), 600, '2019-12-12T20:00:00.000Z'],
    [new Date(dateString), 6000, '2019-12-16T14:00:00.000Z'],
  ])('addMinutes(%s, %i)', (dateTime, minutes, expected) => {
    it(`should return ${expected}`, () => {
      const dateTimePlusMinutes = addMinutes(dateTime, minutes);
      const dateTimePlusMinutesIsoString = dateTimePlusMinutes.toISOString();

      expect(dateTimePlusMinutes.error).toBeFalsy();
      expect(dateTimePlusMinutesIsoString).toBe(expected);
    });
  });

  describe.each([
    [
      new Date(dateString),
      -6,
      'The minutes field must be greater than 0 on utils addMinutes method, received -6',
    ],
    [
      new Date(dateString),
      -60,
      'The minutes field must be greater than 0 on utils addMinutes method, received -60',
    ],
    [
      new Date(dateString),
      -600,
      'The minutes field must be greater than 0 on utils addMinutes method, received -600',
    ],
    [
      new Date(dateString),
      -6000,
      'The minutes field must be greater than 0 on utils addMinutes method, received -6000',
    ],
  ])('addMinutes(%s, %i)', (dateTime, minutes, errorMessage) => {
    it(`should return an error`, () => {
      const dateTimePlusMinutes = addMinutes(dateTime, minutes);

      expect(dateTimePlusMinutes.error).toBeTruthy();
      expect(dateTimePlusMinutes.error[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    [null, 20, 'The dateTime field is required for utils addMinutes method, received null'],
    [undefined, 20, 'The dateTime field is required for utils addMinutes method, received undefined'],
  ])('when passing datetime as %s to addMinutes', (datetime, minutes, errorMessage) => {
    it('should return an error', () => {
      const response = addMinutes(datetime, minutes);

      expect(response.error).toBeTruthy();
      expect(response.error[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    [null, new Date(), 'The minutes field is required for utils addMinutes method, received null'],
    [undefined, new Date(), 'The minutes field is required for utils addMinutes method, received undefined'],
  ])('when passing minutes as %s to addMinutes', (minutes, datetime, errorMessage) => {
    it('should return an error', () => {
      const response = addMinutes(datetime, minutes);

      expect(response.error).toBeTruthy();
      expect(response.error[0].details).toBe(errorMessage);
    });
  });
});
