const { validateUser } = require('../validator');
const { validUser } = require('../../../config/tests/helper');

describe('validateUser test', () => {
  describe('when passing a valid user', () => {
    it('should return is valid equals to true and no validation', async () => {
      const user = { ...validUser.user };
      const { isValid, errors } = await validateUser(user);

      expect(errors).toBeFalsy();
      expect(isValid).toBe(true);
    });
  });

  describe.each([
    ["type", null, "The 'type' field is required"],
    ["type", undefined, "The 'type' field is required"],
    ["type", "", "The 'type' field can't be empty"],
    ["type", 1, "The 'type' field must be a string!"],
    ["name", "", "The 'name' field can't be empty"],
    ["name", null, "The 'name' field is required"],
    ["name", undefined, "The 'name' field is required"],
    ["name", "", "The 'name' field can't be empty"],
    ["name", 1, "The 'name' field must be a string!"],
    ["birth_date", null, "The 'birth date' field is required"],
    ["birth_date", undefined, "The 'birth date' field is required"],
    ["birth_date", "", "The 'birth date' field can't be empty"],
    ["birth_date", 1, "The 'birth date' field must be a string!"],
    ["identifier", null, "The 'identifier' field is required"],
    ["identifier", undefined, "The 'identifier' field is required"],
    ["identifier", "", "The 'identifier' field can't be empty"],
    ["identifier", 1, "The 'identifier' field must be a string!"],
    ["identifier_type", null, "The 'identifier type' field is required"],
    ["identifier_type", undefined, "The 'identifier type' field is required"],
    ["identifier_type", "", "The 'identifier type' field can't be empty"],
    ["identifier_type", 1, "The 'identifier type' field must be a string!"],
    ["password", null, "The 'password' field is required"],
    ["password", undefined, "The 'password' field is required"],
    ["password", "", "The 'password' field can't be empty"],
    ["password", 1, "The 'password' field must be a string!"],
    ["contracts", null, "The 'contracts' field is required"],
    ["contracts", undefined, "The 'contracts' field is required"],
    ["contracts", [], "The 'contracts' field can't be empty"],
    ["address", null, "The 'address' field is required"],
    ["address", undefined, "The 'address' field is required"],
    ["address", [], "The 'address' field can't be empty"],
    ["address_verified", null, "The 'address verified' field must be a boolean"],
    ["address_verified", undefined, "The 'address verified' field must be a boolean"],
    ["address_verified", "", "The 'address verified' field must be a boolean"],
    ["address_verified", 1, "The 'address verified' field must be a boolean"],
    ["address_verified", "1", "The 'address verified' field must be a boolean"],
    ["address_verification_photo", null, "The 'address verification photo' field is required"],
    ["address_verification_photo", undefined, "The 'address verification photo' field is required"],
    ["address_verification_photo", "", "The 'address verification photo' field can't be empty"],
    ["address_verification_photo", 1, "The 'address verification photo' field must be a string!"],
    ["phone", null, "The 'phone' field is required"],
    ["phone", undefined, "The 'phone' field is required"],
    ["phone", "", "The 'phone' field can't be empty"],
    ["phone", 1, "The 'phone' field must be a string!"],
    ["phone_verified", null, "The 'phone verified' field must be a boolean"],
    ["phone_verified", undefined, "The 'phone verified' field must be a boolean"],
    ["phone_verified", "", "The 'phone verified' field must be a boolean"],
    ["phone_verified", 1, "The 'phone verified' field must be a boolean"],
    ["phone_verified", "1", "The 'phone verified' field must be a boolean"],
    ["email", "", "The 'email' field can't be empty"],
    ["email", null, "The 'email' field is required"],
    ["email", undefined, "The 'email' field is required"],
    ["email", "", "The 'email' field can't be empty"],
    ["email", 1, "The 'email' field must be a string!"],
    ["email_verified", null, "The 'email verified' field must be a boolean"],
    ["email_verified", undefined, "The 'email verified' field must be a boolean"],
    ["email_verified", "", "The 'email verified' field must be a boolean"],
    ["email_verified", 1, "The 'email verified' field must be a boolean"],
    ["email_verified", "1", "The 'email verified' field must be a boolean"],
    ["first_login", null, "The 'first login' field must be a boolean"],
    ["first_login", undefined, "The 'first login' field must be a boolean"],
    ["first_login", "", "The 'first login' field must be a boolean"],
    ["first_login", 1, "The 'first login' field must be a boolean"],
    ["first_login", "1", "The 'first login' field must be a boolean"],
    ["welcome_email_sent", null, "The 'welcome email sent' field must be a boolean"],
    ["welcome_email_sent", undefined, "The 'welcome email sent' field must be a boolean"],
    ["welcome_email_sent", "", "The 'welcome email sent' field must be a boolean"],
    ["welcome_email_sent", 1, "The 'welcome email sent' field must be a boolean"],
    ["welcome_email_sent", "1", "The 'welcome email sent' field must be a boolean"],
    ["notifications", null, "The 'notifications' field is required"],
    ["notifications", undefined, "The 'notifications' field is required"],
    ["notifications", [], "The 'notifications' field can't be empty"],
    ["photo", null, "The 'photo' field is required"],
    ["photo", undefined, "The 'photo' field is required"],
    ["photo", "", "The 'photo' field can't be empty"],
    ["photo", 1, "The 'photo' field must be a string!"],
    ["driver_license_number", null, "The 'driver license number' field is required"],
    ["driver_license_number", undefined, "The 'driver license number' field is required"],
    ["driver_license_number", "", "The 'driver license number' field can't be empty"],
    ["driver_license_number", 1, "The 'driver license number' field must be a string!"],
    ["driver_license_register_number", null, "The 'driver license register number' field is required"],
    ["driver_license_register_number", undefined, "The 'driver license register number' field is required"],
    ["driver_license_register_number", "", "The 'driver license register number' field can't be empty"],
    ["driver_license_register_number", 1, "The 'driver license register number' field must be a string!"],
    ["driver_license_first_habilitation_at", null, "The 'driver license first habilitation at' field is required"],
    ["driver_license_first_habilitation_at", undefined, "The 'driver license first habilitation at' field is required"],
    ["driver_license_first_habilitation_at", "", "The 'driver license first habilitation at' field can't be empty"],
    ["driver_license_first_habilitation_at", 1, "The 'driver license first habilitation at' field must be a string!"],
    ["driver_license_expires_at", null, "The 'driver license expires at' field is required"],
    ["driver_license_expires_at", undefined, "The 'driver license expires at' field is required"],
    ["driver_license_expires_at", "", "The 'driver license expires at' field can't be empty"],
    ["driver_license_expires_at", 1, "The 'driver license expires at' field must be a string!"],
    ["driver_license_category", null, "The 'driver license category' field is required"],
    ["driver_license_category", undefined, "The 'driver license category' field is required"],
    ["driver_license_category", "", "The 'driver license category' field can't be empty"],
    ["driver_license_category", 1, "The 'driver license category' field must be a string!"],
    ["driver_license_photo", null, "The 'driver license photo' field is required"],
    ["driver_license_photo", undefined, "The 'driver license photo' field is required"],
    ["driver_license_photo", "", "The 'driver license photo' field can't be empty"],
    ["driver_license_photo", 1, "The 'driver license photo' field must be a string!"],
    ["blocked", null, "The 'blocked' field must be a boolean"],
    ["blocked", undefined, "The 'blocked' field must be a boolean"],
    ["blocked", "", "The 'blocked' field must be a boolean"],
    ["blocked", 1, "The 'blocked' field must be a boolean"],
    ["blocked", "1", "The 'blocked' field must be a boolean"],
    ["blocker_by", null, "The 'blocker by' field is required"],
    ["blocker_by", undefined, "The 'blocker by' field is required"],
    ["blocker_by", "", "The 'blocker by' field can't be empty"],
    ["blocker_by", 1, "The 'blocker by' field must be a string!"],
    ["blocked_reason", null, "The 'blocked reason' field is required"],
    ["blocked_reason", undefined, "The 'blocked reason' field is required"],
    ["blocked_reason", "", "The 'blocked reason' field can't be empty"],
    ["blocked_reason", 1, "The 'blocked reason' field must be a string!"],
  ])(
    'should return an error when passing %s as %s to validateUser',
    (fieldName, fieldValue, errorMessage) => {
      it('', async () => {
        let newUser = { ...validUser.user };
        const newFieldObject = { [fieldName]: fieldValue };
        newUser = { ...newUser, ...newFieldObject };

        const { isValid, errors } = await validateUser(newUser);

        expect(isValid).toBe(false);
        expect(errors).toBeTruthy();
        expect(errors[0].message).toBe(errorMessage);
      });
    }
  );

  describe.each([
    ["contracts", "items", null, "The contracts items field is required"],
    ["contracts", "items", undefined, "The contracts items field is required"],
    ["contracts", "items", 1, "The contracts items field must be a string!"],
    ["address", "first_line", null, "The address first line field is required"],
    ["address", "first_line", undefined, "The address first line field is required"],
    ["address", "first_line", 1, "The address first line field must be a string!"],
    ["address", "complement", null, "The address complement field is required"],
    ["address", "complement", undefined, "The address complement field is required"],
    ["address", "complement", 1, "The address complement field must be a string!"],
    ["address", "city", null, "The address city field is required"],
    ["address", "city", undefined, "The address city field is required"],
    ["address", "city", 1, "The address city field must be a string!"],
    ["address", "state", null, "The address state field is required"],
    ["address", "state", undefined, "The address state field is required"],
    ["address", "state", 1, "The address state field must be a string!"],7
    ["address", "country", null, "The address country field is required"],
    ["address", "country", undefined, "The address country field is required"],
    ["address", "country", 1, "The address country field must be a string!"],
    ["address", "zip_code", null, "The address zip_code field is required"],
    ["address", "zip_code", undefined, "The address zip_code field is required"],
    ["address", "zip_code", 1, "The address zip_code field must be a string!"],
    ["notifications", "reservations_alert", null, "The notifications reservations alert field is required"],
    ["notifications", "reservations_alert", undefined, "The notifications reservations alert field is required"],
    ["notifications", "reservations_alert", "", "The notifications reservations alert field can't be empty"],
    ["notifications", "reservations_alert", 1, "The notifications reservations alert field must be a boolean"],
    ["notifications", "reservations_alert", "1", "The notifications reservations alert field must be a boolean"],
    ["notifications", "ride_alert", null, "The notifications ride alert field is required"],
    ["notifications", "ride_alert", undefined, "The notifications ride alert field is required"],
    ["notifications", "ride_alert", "", "The notifications ride alert field can't be empty"],
    ["notifications", "ride_alert", 1, "The notifications ride alert field must be a boolean"],
    ["notifications", "ride_alert", "1", "The notifications ride alert field must be a boolean"],
  ])('when passing %s.%s as %s to validateUser', (fieldName, nestedFieldName, fieldValue, errorMessage) => {
    it('should return an error', async () => {
      const newUser = { ...validUser.user };
      const newFieldObject = { [nestedFieldName]: fieldValue };

      newUser[fieldName] = [...newUser[fieldName]];
      newUser[fieldName][0] = { ...newUser[fieldName][0], ...newFieldObject };

      const { isValid, errors } = await validateUser(newUser);

      expect(isValid).toBe(false);
      expect(errors).toBeTruthy();
      expect(errors[0].message).toBe(errorMessage);
    });
  });
});
