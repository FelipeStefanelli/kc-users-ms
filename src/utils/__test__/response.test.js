const { createResponse, httpStatusCodes, success, badRequest, internalServerError } = require('../response');

const { validBody, validStatusCode } = require('./helpers/response.helper');

describe('createResponse test suit', () => {
  describe('when passing a valid status code and body to createResponse', () => {
    it('should return a valid response', () => {
      const response = createResponse(validStatusCode, validBody);

      expect(response).toBeTruthy();
      expect(response).toHaveProperty('statusCode');
      expect(response).toHaveProperty('body');
      expect(JSON.parse(response.body)).toHaveProperty('data');
    });
  });

  describe.each([
    [null, validBody, 'The statusCode field is required for utils createResponse method'],
    [undefined, validBody, 'The statusCode field is required for utils createResponse method'],
    ['200', validBody, 'The statusCode field must be a number for utils createResponse method'],
  ])('when passing statusCode as %s to createResponse', (statusCode, body, errorMessage) => {
    it('should return an error', () => {
      const response = createResponse(statusCode, body);

      expect(response.error).toBeTruthy();
      expect(response.error[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    [null, validStatusCode, 'The body field is required for utils createResponse method'],
    [undefined, validStatusCode, 'The body field is required for utils createResponse method'],
    ['Invalid body', validStatusCode, 'The body field must be an object for utils createResponse method'],
  ])('when passing body as %s to createResponse', (body, statusCode, errorMessage) => {
    it('should return an error', () => {
      const response = createResponse(statusCode, body);

      expect(response.error).toBeTruthy();
      expect(response.error[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    ['success', success, validBody, httpStatusCodes.OK],
    ['badRequest', badRequest, validBody, httpStatusCodes.BAD_REQUEST],
    ['internalServerError', internalServerError, validBody, httpStatusCodes.INTERNAL_SERVER_ERROR],
  ])('when calling %s function', (_, responseFunction, body, statusCode) => {
    it('should return a valid response', () => {
      const response = responseFunction(body);

      expect(response).toBeTruthy();
      expect(response).toHaveProperty('statusCode');
      expect(response.statusCode).toBe(statusCode);
      expect(response).toHaveProperty('body');
      expect(JSON.parse(response.body)).toHaveProperty('data');
    });
  });
});
