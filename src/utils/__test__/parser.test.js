const { parseEvent, parseJSON, parseParams, buildParams, buildUpdateParams } = require('../parser');
const {
  validParams,
  validParsedKeyConditionsParams,
  validParsedUpdateParams,
  validUpdateParams,
  validKeyConditionsParams,
} = require('./helpers/parser.helper');
const {
  URL_KEY_CONDITIONS_PARAMS_SEPARATOR,

} = require('../../constants/urlSeparators');

const TestHelper = require('../../../config/tests/helper');

describe('parseEvent test suit', () => {
  describe('when passing a valid event to parseEvent', () => {
    it('should return a body and queryString without errors', () => {
      const event = {
        body: JSON.stringify(TestHelper.validUser),
        queryStringParameters: { teste: 'teste' },
      };
      const response = parseEvent(event);

      expect(response.error).toBeFalsy();
      expect(response.body).toBeTruthy();
      expect(response.queryString).toBeTruthy();
      expect(response.body).toMatchObject(JSON.parse(event.body));
      expect(response.queryString).toMatchObject(event.queryStringParameters);
    });
  });

  describe.each([
    [null, 'The event field is required for utils parseEvent method'],
    [undefined, 'The event field is required for utils parseEvent method'],
  ])('when passing event as %s to parseEvent', (event, errorMessage) => {
    it('should return an error', () => {
      const parsedEvent = parseEvent(event);

      expect(parsedEvent.error).toBeTruthy();
      expect(parsedEvent.error[0].details).toBe(errorMessage);
    });
  });

  describe('when passing an invalid event body to parseEvent', () => {
    it('should return a error', () => {
      const event = {
        body: { teste: 'teste' },
        queryStringParameters: { teste: 'teste' },
      };
      const parsedEvent = parseEvent(event);

      expect(parsedEvent.error).toBeTruthy();
    });
  });

  describe.each([
    [null, {}],
    [undefined, {}],
  ])('when passing event body as %s to parseEvent', (eventBody, expected) => {
    it('should return an empty body object', () => {
      const event = {
        body: eventBody,
        queryStringParameters: { teste: 'teste' },
      };
      const parsedEvent = parseEvent(event);

      expect(parsedEvent.error).toBeFalsy();
      expect(parsedEvent.body).toStrictEqual(expected);
    });
  });

  describe.each([
    [null, {}],
    [undefined, {}],
  ])(
    'when passing event query string parameters as %s to parseEvent',
    (eventQueryStringParameters, expected) => {
      it('should return an empty query string object', () => {
        const event = {
          body: JSON.stringify({ teste: 'teste' }),
          queryStringParameters: eventQueryStringParameters,
        };
        const parsedEvent = parseEvent(event);

        expect(parsedEvent.error).toBeFalsy();
        expect(parsedEvent.queryString).toStrictEqual(expected);
      });
    }
  );
});

describe('parseJSON test suit', () => {
  describe.each([
    [null, 'The stringifiedJson field is required for utils parseJSON method'],
    [undefined, 'The stringifiedJson field is required for utils parseJSON method'],
    ['', 'The stringifiedJson field is required for utils parseJSON method'],
  ])('when passing stringified json as %s to parseJSON', (stringifiedJson, errorMessage) => {
    it('should return an error', () => {
      const parsedJson = parseJSON(stringifiedJson);

      expect(parsedJson.error).toBeTruthy();
      expect(parsedJson.error[0].details).toBe(errorMessage);
    });
  });

  describe('when passing an invalid stringifiedJSON to parseJSON', () => {
    it('should return a error', () => {
      const stringifiedJson = { test: 'Teste' };
      const parsedJson = parseJSON(stringifiedJson);

      expect(parsedJson.error).toBeTruthy();
    });
  });
});

describe('parseParams test suit', () => {
  describe('when passing a valid params object to parseParams', () => {
    it('should return an object with the parsed params', () => {
      const parsedParams = parseParams(validParams);

      expect(parsedParams.error).toBeFalsy();
      expect(parsedParams.keyConditionParams).toStrictEqual(validParsedKeyConditionsParams);
    });
  });

  describe.each([
    [null, {}],
    [undefined, {}],
    ['', {}],
  ])('when passing params as %s to parseParams', (params, result) => {
    it('should return an empty object for each params category', () => {
      const parsedParams = parseParams(params);

      expect(parsedParams.keyConditionParams).toStrictEqual(result);
    });
  });
});

describe('buildParams test suit', () => {

  describe('when passing a valid key conditions params object to buildParams', () => {
    it('should return an object with the processed key conditions params', () => {
      const processedParams = buildParams(validKeyConditionsParams, URL_KEY_CONDITIONS_PARAMS_SEPARATOR);

      expect(processedParams).toStrictEqual(validParsedKeyConditionsParams);
    });
  });
});

describe('buildUpdateParams test suit', () => {
  describe('when passing a valid params object to buildUpdateParams', () => {
    it('should return an object with the parsed update params', () => {
      const processedParams = buildUpdateParams(validUpdateParams);

      expect(processedParams).toStrictEqual(validParsedUpdateParams);
    });
  });
});
