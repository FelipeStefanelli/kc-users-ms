const { t } = require('../utils/i18n');

const buildError = (errors, title = null) => {
  return {
    errors: {
      validation: errors && [...errors],
      title: title || t('request.error'),
    },
  };
};

module.exports = {
  buildError,
};
