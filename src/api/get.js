const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { userSerializer } = require('../model/userSerializer');
const { getByUuid } = require('../database/get');

module.exports.get = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'get', 'api', event), t('request.error'));
  }

  if (!event.pathParameters) {
    return buildError(
      REQUEST_FIELD_REQUIRED('pathParameters', 'get', 'api', event.pathParameters),
      t('request.error')
    );
  }

  if (!event.pathParameters.uuid) {
    return buildError(
      REQUEST_FIELD_REQUIRED('uuid', 'get', 'api', event.pathParameters.uuid),
      t('request.error')
    );
  }

  const { uuid } = event.pathParameters;

  const { data, error } = await getByUuid({ uuid });

  if (error) {
    return buildError(error, t('request.get.error'));
  }

  if (!data) {
    return buildError([{ message: t('request.get.notFound') }], t('request.get.error'));
  }

  const serializedUser = userSerializer(data);

  const response = { data: serializedUser };

  return response;
};
