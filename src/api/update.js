const { t } = require('../utils/i18n');
const { validateUser } = require('../utils/validator');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { userSerializer } = require('../model/userSerializer');
const { updateUser } = require('../database/update');

module.exports.update = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'update', 'api', event), t('request.error'));
  }

  if (!event.body) {
    return buildError(REQUEST_FIELD_REQUIRED('body', 'update', 'api', event.body), t('request.error'));
  }

  if (!event.body.user) {
    return buildError(REQUEST_FIELD_REQUIRED('user', 'update', 'api', event.body.user), t('request.error'));
  }

  if (!event.pathParameters) {
    return buildError(
      REQUEST_FIELD_REQUIRED('pathParameters', 'update', 'api', event.pathParameters),
      t('request.error')
    );
  }

  if (!event.pathParameters.uuid) {
    return buildError(
      REQUEST_FIELD_REQUIRED('uuid', 'update', 'api', event.pathParameters.uuid),
      t('request.error')
    );
  }

  const { user } = event.body;
  const { uuid } = event.pathParameters;
  const isUpdate = true;

  const { isValid, errors } = await validateUser(user, isUpdate);
  if (!isValid) {
    return buildError(errors, t('validation.error'));
  }

  const { data, error } = await updateUser(user, { uuid });
  if (error) {
    return buildError(error, t('request.update.error'));
  }

  const serializedUser = userSerializer(data);

  const response = { data: serializedUser };

  return response;
};
