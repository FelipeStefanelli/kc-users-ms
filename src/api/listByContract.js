const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { parseParams } = require('../utils/parser');
const { listByContract } = require('../database/listByContract');

module.exports.listByContract = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'listByContract', 'api', event), t('request.error'));
  }

  if (!event.queryString) {
    return buildError(
      REQUEST_FIELD_REQUIRED('queryString', 'listByContract', 'api', event.queryString),
      t('request.error')
    );
  }

  const { keyConditionParams, error } = parseParams(event.queryString);

  if (error) {
    return buildError(error, t('request.listByContract.error'));
  }

  const { data, errors } = await listByContract(keyConditionParams);
  if (errors) {
    return buildError(errors, t('request.listByContract.error'));
  }

  const response = { data };

  return response;
};