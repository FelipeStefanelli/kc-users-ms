const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { login } = require('../database/login');

module.exports.login = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'login', 'api', event), t('request.error'));
  }
  console.log(event)
  if (!event.headers.Authorization) {
    return buildError(REQUEST_FIELD_REQUIRED('event headers authorization', 'login', 'api', event.headers.Authorization), t('request.error'));
  }

  if (!event.headers['api-token']) {
    return buildError(REQUEST_FIELD_REQUIRED('event headers api-token', 'login', 'api', event.headers['api-token']), t('request.error'));
  }

  if (!event.headers['refresh-token-expire']) {
    return buildError(REQUEST_FIELD_REQUIRED('event query string param refresh-token-expire', 'login', 'api', event.headers['refresh-token-expire']), t('request.error'));
  }

  const accessToken = event.headers.Authorization;
  const apiToken = event.headers['api-token'];
  const refreshTokenExpire = event.headers['refresh-token-expire'];
  const dividedToken = accessToken.split(' ');
  const authType = dividedToken[0];
  const encodeToken = dividedToken[1];

  const decodedToken = Buffer.from(encodeToken, 'base64').toString().split(':');
  const email = {
    ComparisonOperator: 'EQ',
    AttributeValueList: [decodedToken[0]]
  };
  const password = decodedToken[1];

  const { data, errors } = await login(email, password, apiToken, refreshTokenExpire);
  if (errors) {
    return buildError(errors, t('request.login.error'));
  }

  const response = { data };

  return response;
};