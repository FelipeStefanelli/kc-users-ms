const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { refresh } = require('../database/refresh');

module.exports.refresh = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'refresh', 'api', event), t('request.error'));
  }

  if (!event.headers['api-token']) {
    return buildError(REQUEST_FIELD_REQUIRED('event headers api-token', 'refresh', 'api', event.headers['api-token']), t('request.error'));
  }

  if (!event.headers['access-token']) {
    return buildError(REQUEST_FIELD_REQUIRED('event headers access-token', 'refresh', 'api', event.headers['access-token']), t('request.error'));
  }

  if (!event.headers['refresh-token']) {
    return buildError(REQUEST_FIELD_REQUIRED('event headers refresh-token', 'refresh', 'api', event.headers['refresh-token']), t('request.error'));
  }

  if (!event.headers['refresh-token-expire']) {
    return buildError(REQUEST_FIELD_REQUIRED('event query string param refresh-token-expire', 'login', 'api', event.headers['refresh-token-expire']), t('request.error'));
  }

  const refreshTokenExpire = event.headers['refresh-token-expire'];
  const accessToken = event.headers['access-token'];
  const refreshToken = event.headers['refresh-token'];
  const apiToken = event.headers['api-token'];

  const { data, errors } = await refresh(accessToken, refreshToken, apiToken, refreshTokenExpire);
  if (errors) {
    return buildError(errors, t('request.refresh.error'));
  }

  const response = { data };

  return response;
};