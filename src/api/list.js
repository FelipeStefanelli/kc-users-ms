const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { parseParams } = require('../utils/parser');
const { list } = require('../database/list');

module.exports.list = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'list', 'api', event), t('request.error'));
  }

  if (!event.queryString) {
    return buildError(
      REQUEST_FIELD_REQUIRED('queryString', 'list', 'api', event.queryString),
      t('request.error')
    );
  }

  const { keyConditionParams, error } = parseParams(event.queryString);

  if (error) {
    return buildError(error, t('request.list.error'));
  }

  const { data, errors } = await list(keyConditionParams);
  if (errors) {
    return buildError(errors, t('request.list.error'));
  }

  const response = { data };

  return response;
};