const { createUser } = require('../../database/create');
const { get } = require('../../api/get');

const TestHelper = require('../../../config/tests/helper');
const { buildRequiredErrorMessage } = require('../../database/__test__/helpers/messages.helper');

let createdUser;
let userUuid;

describe('Api get method test suit', () => {
  beforeEach(async () => {
    const { user } = TestHelper.validUser;
    const { data } = await createUser(user);
    const { uuid } = data;

    createdUser = data;
    userUuid = uuid;
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when getting a user with api get method', () => {
    it('should return user data', async () => {
      const { uuid } = createdUser;

      const pathParameters = {
        uuid
      };
      const { data, errors } = await get({ pathParameters });

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();

      expect(data).toStrictEqual(createdUser);
    });
  });

  describe.each([
    ['111', 'User not found'],
    ['222', 'User not found'],
    ['333', 'User not found'],
    ['a', 'User not found'],
    ['b', 'User not found'],
    ['c', 'User not found'],
  ])('when passing non existing uuid as %s to api get method', (userUuid, errorMessage) => {
    it('should return an error', async () => {
      const pathParameters = {
        uuid: userUuid,
      };
      const { data, errors } = await get({ pathParameters });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.validation[0].message).toBe(errorMessage);
    });
  });


  describe.each([
    ['', buildRequiredErrorMessage('uuid', '', 'api', 'get')],
    [null, buildRequiredErrorMessage('uuid', null, 'api', 'get')],
    [undefined, buildRequiredErrorMessage('uuid', undefined, 'api', 'get')],
  ])('when passing uuid as %s to api get method', (userUuid, errorMessage) => {
    it('should return an error', async () => {
      const pathParameters = {
        uuid: userUuid
      };

      const { data, errors } = await get({ pathParameters });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.validation[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing queryString as %s to api get method', (pathParametersValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await get({ pathParameters: pathParametersValue });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing event as %s to api get method', (eventValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await get(eventValue);

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });
});
