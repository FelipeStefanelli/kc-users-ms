const { createUser } = require('../../database/create');
const { list } = require('../list');

const TestHelper = require('../../../config/tests/helper');
const {
  buildKeyConditionsOwnerRequiredErrorMessage,
} = require('../../database/__test__/helpers/messages.helper');

const createdUser = [];

const addUser = async (type) => {
  const { user } = TestHelper.validUser;
  const newUser = { ...user };
  newUser.type = type;
  const { data } = await createUser(newUser);
  createdUser.push(data);
};

describe('Api list method test suit', () => {
  beforeAll(async () => {
    await Promise.all([
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
    ]);
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when listing users with api list method', () => {
    it('should return users data array', async () => {
      const queryString = {
        'type@EQ': 'user',
      };
      const { data, errors } = await list({ queryString });

      const { users } = data;

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();

      expect(users).toHaveLength(10);

      for (let i = 0; i < 10; i += 1) {
        expect(users[i].type).toBe('user');
      }
    });
  });

  describe('when listing users with non existing type with api list method', () => {
    it('should return empty users array', async () => {
      const queryString = {
        'type@EQ': 'user',
      };
      const { data, errors } = await list({ queryString });

      const { users } = data;

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();

      expect(users).toHaveLength(0);
    });
  });

  describe('when listing users with invalid params with api list method', () => {
    it('should return an error', async () => {
      const queryString = {
        'range@EQ': 'abc',
      };
      const { data, errors } = await list({ queryString });

      expect(errors).toBeTruthy();
      expect(data).toBeFalsy();
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing queryString as %s to api list method', (queryStringValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await list({ queryString: queryStringValue });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing event as %s to api list method', (eventValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await list(eventValue);

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });
});
