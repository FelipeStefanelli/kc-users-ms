const { createUser } = require('../../database/create');
const { listByContract } = require('../listByContract');

const TestHelper = require('../../../config/tests/helper');

const createdUser = [];

const addUser = async (contract) => {
  const { user } = TestHelper.validUser;
  const newUser = { ...user };
  newUser.contract = contract;
  const { data } = await createUser(newUser);
  createdUser.push(data);
};

describe('Api listByContract method test suit', () => {
  beforeAll(async () => {
    await Promise.all([
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
    ]);
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when listByContracting users with api listByContract method', () => {
    it('should return users data array', async () => {
      const queryString = {
        'contract@EQ': 'user',
      };
      const { data, errors } = await listByContract({ queryString });

      const { users } = data;

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();

      expect(users).toHaveLength(10);

      for (let i = 0; i < 10; i += 1) {
        expect(users[i].contract).toBe('user');
      }
    });
  });

  describe('when listByContracting users with non existing contract with api listByContract method', () => {
    it('should return empty users array', async () => {
      const queryString = {
        'contract@EQ': 'user',
      };
      const { data, errors } = await listByContract({ queryString });

      const { users } = data;

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();

      expect(users).toHaveLength(0);
    });
  });

  describe('when listByContracting users with invalid params with api listByContract method', () => {
    it('should return an error', async () => {
      const queryString = {
        'range@EQ': 'abc',
      };
      const { data, errors } = await listByContract({ queryString });

      expect(errors).toBeTruthy();
      expect(data).toBeFalsy();
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing queryString as %s to api listByContract method', (queryStringValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await listByContract({ queryString: queryStringValue });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing event as %s to api listByContract method', (eventValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await listByContract(eventValue);

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });
});
