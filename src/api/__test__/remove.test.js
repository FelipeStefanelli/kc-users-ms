const { createUser } = require('../../database/create');
const { remove } = require('../../api/remove');

const TestHelper = require('../../../config/tests/helper');
const { buildRequiredErrorMessage } = require('../../database/__test__/helpers/messages.helper');

let userUuid;

describe('Api remove method test suit', () => {
  beforeEach(async () => {
    const { user } = TestHelper.validUser;
    const { data } = await createUser(user);
    const { uuid } = data;

    userUuid = uuid.split('_')[1];
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when removing a user with api remove method', () => {
    it('should return true', async () => {
      const pathParameters = {
        uuid: userUuid
      };
      const { data, errors } = await remove({ pathParameters });

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();
    });
  });

  describe.each([
    ['', buildRequiredErrorMessage('uuid', '', 'api', 'remove')],
    [null, buildRequiredErrorMessage('uuid', null, 'api', 'remove')],
    [undefined, buildRequiredErrorMessage('uuid', undefined, 'api', 'remove')],
  ])('when passing uuid as %s to api remove method', (userUuid, errorMessage) => {
    it('should return an error', async () => {
      const pathParameters = {
        uuid: userUuid
      };

      const { data, errors } = await remove({ pathParameters });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.validation[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing queryString as %s to api get method', (pathParametersValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await remove({ pathParameters: pathParametersValue });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });

  describe.each([
    ['', 'The request has failed, please try again'],
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
  ])('when passing event as %s to api get method', (eventValue, errorMessage) => {
    it('should return an error', async () => {
      const { data, errors } = await remove(eventValue);

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorMessage);
    });
  });
});
