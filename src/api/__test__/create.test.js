const { create } = require('../create');
const TestHelper = require('../../../config/tests/helper');

const {
  emptyType,
  requiredType,
  emptyName,
  requiredName,
  emptyBirthDate,
  requiredBirthDate,
  emptyIdentifier,
  requiredIdentifier,
  emptyIdentifierType,
  requiredIdentifierType,
  emptyPassword,
  requiredPassword,
  emptyContracts,
  requiredContracts,
  emptyAdress,
  requiredAdress,
  requiredAdressVerified,
  emptyAdressVerificationPhoto,
  requiredAdressVerificationPhoto,
  emptyPhone,
  requiredPhone,
  requiredPhoneVerified,
  emptyEmail,
  requiredEmail,
  requiredEmailVerified,
  requiredFirstLogin,
  requiredWelcomeEmailSent,
  emptyNotifications,
  requiredNotifications,
  emptyPhoto,
  requiredPhoto,
  emptyDriverLicenseNumber,
  requiredDriverLicenseNumber,
  emptyDriverLicenseRegisterNumber,
  requiredDriverLicenseRegisterNumber,
  emptyDriverLicenseFirstHabilitationAt,
  requiredDriverLicenseFirstHabilitationAt,
  emptyDriverLicenseExpiresAt,
  requiredDriverLicenseExpiresAt,
  emptyDriverLicenseCategory,
  requiredDriverLicenseCategory,
  emptyDriveLicensePhoto,
  requiredDriverLicensePhoto,
  requiredBlocked,
  emptyBlockedBy,
  requiredBlockedBy,
  emptyBlockedReason,
  requiredBlockedReason,
} = TestHelper.validationErrors;

const { validUser } = TestHelper;

describe('Api create method test suit', () => {
  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when passing a valid body to create method', () => {
    it('should return user data', async () => {
      const body = validUser;

      const { data, errors } = await create({ body });

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();

      expect(data).toMatchObject(body.user);
    });

    it('should return an error', async () => {
      const body = validUser;

      await TestHelper.deleteTables();

      const { data, errors } = await create({ body });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();

      await TestHelper.createTables();
    });
  });

  describe.each([
    [ 'type', '', emptyType ],
    [ 'type', null, requiredType ],
    [ 'type', undefined, requiredType ],
    [ 'name', '', emptyName ],
    [ 'name', null, requiredName ],
    [ 'name', undefined, requiredName ],
    [ 'birth_date', '', emptyBirthDate ],
    [ 'birth_date', null, requiredBirthDate ],
    [ 'birth_date', undefined, requiredBirthDate ],
    [ 'identifier', '', emptyIdentifier ],
    [ 'identifier', null, requiredIdentifier ],
    [ 'identifier', undefined, requiredIdentifier ],
    [ 'identifier_type', '', emptyIdentifierType ],
    [ 'identifier_type', null, requiredIdentifierType ],
    [ 'identifier_type', undefined, requiredIdentifierType ],
    [ 'password', '', emptyPassword ],
    [ 'password', null, requiredPassword ],
    [ 'password', undefined, requiredPassword ],
    [ 'contracts', null, requiredContracts ],
    [ 'contracts', undefined, requiredContracts ],
    [ 'contracts', , emptyContracts ],
    [ 'adress', '', emptyAdress ],
    [ 'adress', null, requiredAdress ],
    [ 'adress', undefined, requiredAdress ],
    [ 'adress_verified', '', requiredAdressVerified ],
    [ 'address_verification_photo', null, requiredAdressVerificationPhoto ],
    [ 'address_verification_photo', undefined, requiredAdressVerificationPhoto ],
    [ 'address_verification_photo', '', emptyAdressVerificationPhoto ],
    [ 'phone', '', emptyPhone ],
    [ 'phone', null, requiredPhone ],
    [ 'phone', undefined, requiredPhone ],
    [ 'phone_verified', '', requiredPhoneVerified ],
    [ 'email', '', emptyEmail ],
    [ 'email', null, requiredEmail ],
    [ 'email', undefined, requiredEmail ],
    [ 'email_verified', '', requiredEmailVerified ],
    [ 'first_login', '', requiredFirstLogin ],
    [ 'first_login', null, requiredFirstLogin ],
    [ 'first_login', undefined, requiredFirstLogin ],
    [ 'welcome_email_sent', '', requiredWelcomeEmailSent ],
    [ 'welcome_email_sent', null, requiredWelcomeEmailSent ],
    [ 'welcome_email_sent', undefined, requiredWelcomeEmailSent ],
    [ 'notifications', null, requiredNotifications ],
    [ 'notifications', undefined, requiredNotifications ],
    [ 'notifications', , emptyNotifications ],
    [ 'photo', '', emptyPhoto ],
    [ 'photo', null, requiredPhoto ],
    [ 'photo', undefined, requiredPhoto ],
    [ 'driver_license_number', '', emptyDriverLicenseNumber ],
    [ 'driver_license_number', null, requiredDriverLicenseNumber ],
    [ 'driver_license_number', undefined, requiredDriverLicenseNumber ],
    [ 'driver_license_register_number', '', emptyDriverLicenseRegisterNumber ],
    [ 'driver_license_register_number', null, requiredDriverLicenseRegisterNumber ],
    [ 'driver_license_register_number', undefined, requiredDriverLicenseRegisterNumber ],
    [ 'driver_license_first_habilitation_at', '', emptyDriverLicenseFirstHabilitationAt ],
    [ 'driver_license_first_habilitation_at', null, requiredDriverLicenseFirstHabilitationAt ],
    [ 'driver_license_first_habilitation_at', undefined, requiredDriverLicenseFirstHabilitationAt ],
    [ 'driver_license_expires_at', '', emptyDriverLicenseExpiresAt ],
    [ 'driver_license_expires_at', null, requiredDriverLicenseExpiresAt ],
    [ 'driver_license_expires_at', undefined, requiredDriverLicenseExpiresAt ],
    [ 'driver_license_category', '', emptyDriverLicenseCategory ],
    [ 'driver_license_category', null, requiredDriverLicenseCategory ],
    [ 'driver_license_category', undefined, requiredDriverLicenseCategory ],
    [ 'driver_license_photo', '', emptyDriveLicensePhoto ],
    [ 'driver_license_photo', null, requiredDriverLicensePhoto ],
    [ 'driver_license_photo', undefined, requiredDriverLicensePhoto ],
    [ 'blocked', '', requiredBlocked ],
    [ 'blocked', null, requiredBlocked ],
    [ 'blocked', undefined, requiredBlocked ],
    [ 'blocked_by', '', emptyBlockedBy ],
    [ 'blocked_by', null, requiredBlockedBy ],
    [ 'blocked_by', undefined, requiredBlockedBy ],
    [ 'block_reason', '', emptyBlockedReason ],
    [ 'block_reason', null, requiredBlockedReason ],
    [ 'block_reason', undefined, requiredBlockedReason ]
  ])('when passing invalid %s as %s to create method', (field, fieldValue, errorObject) => {
    it('should return an error', async () => {
      const newUser = { ...validUser.user };
      newUser[field] = fieldValue;
      const body = { user: newUser };

      const { data, errors } = await create({ body });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.validation[0]).toMatchObject(errorObject);
    });
  });

  describe.each([
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
    [{}, 'The request has failed, please try again'],
  ])('when passing invalid body as %s to create method', (body, errorTitle) => {
    it('should return an error', async () => {
      const { data, errors } = await create({ body });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorTitle);
    });
  });
});
