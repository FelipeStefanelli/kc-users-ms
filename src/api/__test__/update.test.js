const { createUser } = require('../../database/create');
const { getByUuid } = require('../../database/get');
const { update } = require('../update');
const { validContracts, validNotifications } = require('../../database/__test__/helpers/update.helper');

const TestHelper = require('../../../config/tests/helper');

let userUuid;
let validUser;

const {
  emptyType,
  requiredType,
  emptyName,
  requiredName,
  emptyBirthDate,
  requiredBirthDate,
  emptyIdentifier,
  requiredIdentifier,
  emptyIdentifierType,
  requiredIdentifierType,
  emptyPassword,
  requiredPassword,
  emptyContracts,
  requiredContracts,
  emptyAdress,
  requiredAdress,
  requiredAdressVerified,
  emptyAdressVerificationPhoto,
  requiredAdressVerificationPhoto,
  emptyPhone,
  requiredPhone,
  requiredPhoneVerified,
  emptyEmail,
  requiredEmail,
  requiredEmailVerified,
  requiredFirstLogin,
  requiredWelcomeEmailSent,
  emptyNotifications,
  requiredNotifications,
  emptyPhoto,
  requiredPhoto,
  emptyDriverLicenseNumber,
  requiredDriverLicenseNumber,
  emptyDriverLicenseRegisterNumber,
  requiredDriverLicenseRegisterNumber,
  emptyDriverLicenseFirstHabilitationAt,
  requiredDriverLicenseFirstHabilitationAt,
  emptyDriverLicenseExpiresAt,
  requiredDriverLicenseExpiresAt,
  emptyDriverLicenseCategory,
  requiredDriverLicenseCategory,
  emptyDriveLicensePhoto,
  requiredDriverLicensePhoto,
  requiredBlocked,
  emptyBlockedBy,
  requiredBlockedBy,
  emptyBlockedReason,
  requiredBlockedReason,
} = TestHelper.validationErrors;

describe('Data base actions test suit', () => {
  beforeEach(async () => {
    const { user } = TestHelper.validUser;
    const { data } = await createUser(user);
    const { uuid } = data;

    validUser = user;
    userUuid = uuid.split('_')[1];
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe.each([
    [ 'type', 'user' ],
    [ 'type', 'contract' ],
    [ 'name', 'Felipe' ],
    [ 'name', 'Joao' ],
    [ 'birth_date', '1995-03-19' ],
    [ 'birth_date', '1988-06-07' ],
    [ 'identifier', '1324569811' ],
    [ 'identifier', 'lalalala' ],
    [ 'identifier_type', 'cpf' ],
    [ 'identifier_type', 'passaporte' ],
    [ 'password', 'M2kdaai2djaUHS4Adsa' ],
    [ 'password', 'uLoisbc14grs3' ],
    [ 'contracts', validContracts ],
    [ 'adress', 'Belo Horizonte' ],
    [ 'adress', 'rua x' ],
    [ 'adress_verified', '' ],
    [ 'address_verification_photo', 'url' ],
    [ 'address_verification_photo', 'http://xxxx.com.br' ],
    [ 'phone', '+5599123456789' ],
    [ 'phone', '+5599573482489' ],
    [ 'phone_verified', false ],
    [ 'email', 'xxxxx' ],
    [ 'email', 'eva@brewer.com.br' ],
    [ 'email_verified', true ],
    [ 'first_login', false ],
    [ 'first_login', true ],
    [ 'welcome_email_sent', false ],
    [ 'welcome_email_sent', true ],
    [ 'notifications', validNotifications ],
    [ 'photo', 'url' ],
    [ 'photo', 'http://lalalal.com' ],
    [ 'driver_license_number', '1234567890' ],
    [ 'driver_license_number', '1287692018' ],
    [ 'driver_license_register_number', '1234567890' ],
    [ 'driver_license_register_number', '1287692018' ],
    [ 'driver_license_first_habilitation_at', '2020-12-30' ],
    [ 'driver_license_expires_at', '2020-12-30' ],
    [ 'driver_license_category', 'A' ],
    [ 'driver_license_category', 'ABCD' ],
    [ 'driver_license_photo', 'url' ],
    [ 'driver_license_photo', 'http:yrl.com.br' ],
    [ 'blocked', false ],
    [ 'blocked', true ],
    [ 'blocked_by', 'user_1d7f2955-a910-4835-b476-b34cce9e224c' ],
    [ 'block_reason', 'laranja' ],
    [ 'block_reason', 'melancia' ],
  ])('when passing %s as %j to updateuser', (fieldName, fieldValue) => {
    it('should return updated user data', async () => {
      const newUser = { ...validUser };
      newUser[fieldName] = fieldValue;

      delete newUser.uuid;

      const body = { user: newUser };
      const pathParameters = { uuid: userUuid };

      const { errors } = await update({ body, pathParameters });

      const { data } = await getByUuid({ uuid: `user_${userUuid}` });

      expect(errors).toBeFalsy();
      expect(data).toMatchObject(newUser);
    });
  });


  describe.each([
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
    [{}, 'The request has failed, please try again'],
    ['', 'The request has failed, please try again'],
  ])('when passing invalid body as %j to update action', (body, errorTitle) => {
    it('should return an error', async () => {
      const { data, errors } = await update({ body, pathParameters: { uuid: 'user_1d7f2955-a910-4835-b476-b34cce9e224c' } });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorTitle);
    });
  });

  describe.each([
    [null, 'The request has failed, please try again'],
    [undefined, 'The request has failed, please try again'],
    ['', 'The request has failed, please try again'],
  ])('when passing invalid path parameters as %j to update action', (pathParameters, errorTitle) => {
    it('should return an error', async () => {
      const { data, errors } = await update({ body: { user: {} }, pathParameters });

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorTitle);
    });
  });
});
