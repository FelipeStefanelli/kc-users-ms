const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { removeUser } = require('../database/remove');

module.exports.remove = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'remove', 'api', event), t('request.error'));
  }

  if (!event.pathParameters) {
    return buildError(
      REQUEST_FIELD_REQUIRED('pathParameters', 'remove', 'api', event.pathParameters),
      t('request.error')
    );
  }

  if (!event.pathParameters.uuid) {
    return buildError(
      REQUEST_FIELD_REQUIRED('uuid', 'remove', 'api', event.pathParameters.uuid),
      t('request.error')
    );
  }
  
  const { uuid } = event.pathParameters;

  const { error } = await removeUser({ uuid });

  if (error) {
    return buildError(error, t('request.remove.error'));
  }

  const response = { data: t('request.remove.success') };
  
  return response;
};
