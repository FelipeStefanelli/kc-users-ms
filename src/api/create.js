const { t } = require('../utils/i18n');
const { validateUser } = require('../utils/validator');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { userSerializer } = require('../model/userSerializer');
const { createUser } = require('../database/create');

module.exports.create = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'create', 'api', event), t('request.error'));
  }

  if (!event.body) {
    return buildError(REQUEST_FIELD_REQUIRED('body', 'create', 'api', event.body), t('request.error'));
  }

  if (!event.body.user) {
    return buildError(REQUEST_FIELD_REQUIRED('user', 'create', 'api', event.body.user), t('request.error'));
  }

  const { user } = event.body;

  const { isValid, errors } = await validateUser(user);
  if (!isValid) {
    return buildError(errors, t('validation.error'));
  }

  const { data, error } = await createUser(user);
  if (error) {
    return buildError(error, t('request.create.error'));
  }

  const serializedUser = userSerializer(data);

  const response = { data: serializedUser };

  return response;
};
