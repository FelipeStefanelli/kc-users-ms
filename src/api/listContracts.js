const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { parseParams } = require('../utils/parser');
const { listContracts } = require('../database/listContracts');

module.exports.listContracts = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'listContracts', 'api', event), t('request.error'));
  }

  if (!event.queryString) {
    return buildError(
      REQUEST_FIELD_REQUIRED('queryString', 'listContracts', 'api', event.queryString),
      t('request.error')
    );
  }

  const { keyConditionParams, error } = parseParams(event.queryString);

  if (error) {
    return buildError(error, t('request.listContracts.error'));
  }

  const { data, errors } = await listContracts(keyConditionParams);
  if (errors) {
    return buildError(errors, t('request.listContracts.error'));
  }

  const response = { data };

  return response;
};