const { t } = require('../utils/i18n');
const { buildError } = require('../utils/errors');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { authorizer } = require('../database/authorizer');

module.exports.authorizer = async (event) => {
  if (!event) {
    return buildError(REQUEST_FIELD_REQUIRED('event', 'authorizer', 'api', event), t('request.error'));
  }

  if (!event.headers.Authorization) {
    return buildError(REQUEST_FIELD_REQUIRED('event headers authorization', 'authorizer', 'api', event.headers.Authorization), t('request.error'));
  }

  if (!event.headers['api-token']) {
    return buildError(REQUEST_FIELD_REQUIRED('event headers api-token', 'authorizer', 'api', event.headers['api-token']), t('request.error'));
  }
  
  const accessToken = event.headers.Authorization;
  const apiToken = event.headers['api-token'];

  const { data, errors } = await authorizer(accessToken, apiToken);
  if (errors) {
    return buildError(errors, t('request.authorizer.error'));
  }

  const response = { data };

  return response;
};