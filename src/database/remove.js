const { tableName, dynamoDb } = require('../../config/dynamodb');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');

const { updateObject } = require('./update');

const removeUser = async ({ uuid }) => {
  if (!uuid) {
    return { error: REQUEST_FIELD_REQUIRED('uuid', 'removeUser', 'database', uuid) };
  }
  const params = {
    TableName: tableName,
    Key: {
      uuid
    },
    Expected: {
      uuid: {
        Exists: true,
        Value: uuid,
      }
    },
    ReturnValues: 'ALL_OLD',
  };

  const promises = [];

  const data = await dynamoDb
    .delete(params)
    .promise()
    .catch((error) => {
      console.error(error);
      return { error: error.message };
    });
    console.log("DATA:", data)
  if (data.error) {
    return { error: [{ details: data.error }] };
  }

  const { contracts } = data.Attributes;

  contracts.forEach((contract) =>
    promises.push(updateObject(uuid, contract, 'REMOVE_USER'))
  );

  await Promise.all(promises);

  return { data: true };
};

module.exports = {
  removeUser,
};
