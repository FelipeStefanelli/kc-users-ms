const { tableName, dynamoDb } = require('../../config/dynamodb');
const { KEY_CONDITIONS_EMAIL_REQUIRED, KEY_CONDITIONS_PASSWORD_REQUIRED, KEY_CONDITIONS_APITOKEN_REQUIRED, KEY_CONDITIONS_REFRESHTOKENEXPIRE_REQUIRED } = require('../constants/errors');
const jwt = require('jsonwebtoken');
const { updateUser } = require('../database/update');

const login = async (email, password, apiToken, refreshTokenExpire) => {
  if (!email) {
    return { errors: KEY_CONDITIONS_EMAIL_REQUIRED(email) };
  }
  if (!password) {
    return { errors: KEY_CONDITIONS_PASSWORD_REQUIRED(password) };
  }
  if (!apiToken) {
    return { errors: KEY_CONDITIONS_APITOKEN_REQUIRED(apiToken) };
  }
  if (!refreshTokenExpire) {
    return { errors: KEY_CONDITIONS_REFRESHTOKENEXPIRE_REQUIRED(refreshTokenExpire) };
  }
  const params = {
    TableName: tableName,
    IndexName: "emailIndex",
    KeyConditions: {
      email: email,
      uuid: {
        ComparisonOperator: 'BEGINS_WITH',
        AttributeValueList: ['user'],
      },
    },
  };

  const data = await dynamoDb
    .query(params)
    .promise()
    .catch((error) => {
      console.error(error);
      return { error: error.message };
    });

  if (data.error) {
    return { errors: [{ details: data.error }] };
  }

  const { Items } = data;

  if (Items[0]) {
    const expectedPassword = Buffer.from(Items[0].password, 'base64').toString();
    if (password === expectedPassword && apiToken === 'abc123456') {
      console.log(`Aprovado, Senha: ${password}`);
      data.Items[0]['access-token'] = null;

      let accessToken = jwt.sign({
        data: Items[0]
      }, apiToken, { expiresIn: '1h' });

      var refreshToken = jwt.sign({
        data: {
          email
        }
      }, apiToken, { expiresIn: refreshTokenExpire });
      var userUuid = Items[0].uuid;

      if (!Items[0]["access-token"] && !Items[0]["refresh-token"]) {
        var info = {
          "access-token": Buffer.from(accessToken).toString('base64'),
          "refresh-token": [Buffer.from(refreshToken).toString('base64')]
        }
        await updateUser(info, { uuid: userUuid });
      } else {
        var userRefreshToken = Items[0]["refresh-token"];
        const validRefreshTokens = []
        userRefreshToken.map(token => {
          jwt.verify(Buffer.from(token, 'base64').toString(), apiToken, function (err) {
            if (err) {
              return
            } else {
              validRefreshTokens.push(token);
            }
          });
        });

        var info = {
          "access-token": Buffer.from(accessToken).toString('base64'),
          "refresh-token": [...validRefreshTokens, Buffer.from(refreshToken).toString('base64')]
        };
        await updateUser(info, { uuid: userUuid });
      };
      return {
        data: {
          accessToken: accessToken,
          refreshToken: refreshToken
        }
      };
    } else {
      return {
        data: {
          error: "Não autorizado!"
        }
      };
    }
  } else {
    return { errors: [{ details: 'Os dados de autentificação estão incorretos!' }] };
  }
};

module.exports = {
  login
};
