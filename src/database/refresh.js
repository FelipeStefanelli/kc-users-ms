const { tableName, dynamoDb } = require('../../config/dynamodb');
const { KEY_CONDITIONS_TOKEN_REQUIRED, KEY_CONDITIONS_APITOKEN_REQUIRED, KEY_CONDITIONS_REFRESHTOKEN_REQUIRED, KEY_CONDITIONS_REFRESHTOKENEXPIRE_REQUIRED } = require('../constants/errors');
const jwt = require('jsonwebtoken');
const { updateUser } = require('../database/update');

const refresh = async (accessToken, refreshToken, apiToken, refreshTokenExpire) => {
    if (!accessToken) {
        return { errors: KEY_CONDITIONS_TOKEN_REQUIRED(accessToken) };
    }
    if (!apiToken) {
        return { errors: KEY_CONDITIONS_APITOKEN_REQUIRED(apiToken) };
    }
    if (!refreshToken) {
        return { errors: KEY_CONDITIONS_REFRESHTOKEN_REQUIRED(refreshToken) };
    }
    if (!refreshTokenExpire) {
        return { errors: KEY_CONDITIONS_REFRESHTOKENEXPIRE_REQUIRED(refreshTokenExpire) };
    }
    let legit = jwt.verify(accessToken, apiToken);
    let email = '';
    let password = '';
    if (legit.data) {
        email = {
            ComparisonOperator: 'EQ',
            AttributeValueList: [legit.data.email]
        };
        password = legit.data.password;
    };
    const params = {
        TableName: tableName,
        IndexName: "emailIndex",
        KeyConditions: {
            email: email,
            uuid: {
                ComparisonOperator: 'BEGINS_WITH',
                AttributeValueList: ['user'],
            },
        },
    };

    const data = await dynamoDb
        .query(params)
        .promise()
        .catch((error) => {
            console.error(error);
            return { error: error.message };
        });

    if (data.error) {
        return { errors: [{ details: data.error }] };
    }

    const { Items } = data;
    let refreshTokenEncoded = Buffer.from(refreshToken).toString('base64');
    if (password === Items[0].password && email.AttributeValueList[0] === Items[0].email && Items[0]["refresh-token"].indexOf(refreshTokenEncoded) >= 0 && apiToken === "abc123456") {
        var userUuid = Items[0].uuid;
        let newAccessToken = jwt.sign({
            data: Items[0]
        }, apiToken, { expiresIn: '1h' });
        let userRefreshToken = Items[0]["refresh-token"];
        var newRefreshToken = jwt.sign({
            data: {
                email
            }
        }, apiToken, { expiresIn: refreshTokenExpire });
        userRefreshToken.splice(userRefreshToken.indexOf(refreshToken), 1);
        const validRefreshTokens = []
        userRefreshToken.map(token => {
            jwt.verify(Buffer.from(token, 'base64').toString(), apiToken, function (err) {
                if (err) {
                    return
                } else {
                    validRefreshTokens.push(token);
                }
            });
        });
        var info = {
            "access-token": Buffer.from(newAccessToken).toString('base64'),
            "refresh-token": [...validRefreshTokens, Buffer.from(newRefreshToken).toString('base64')]
        };
        await updateUser(info, { uuid: userUuid });
        return {
            data: {
                users: email.AttributeValueList[0],
                accessToken: newAccessToken,
                refreshToken: newRefreshToken
            }
        };
    } else {
        return { errors: [{ details: 'Os dados de Autentificação estão incorretos!' }] };
    }
};

module.exports = {
    refresh
};