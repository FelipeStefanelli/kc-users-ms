const buildRequiredErrorMessage = (fieldName, fieldValue, folderName, methodName) =>
  `The ${fieldName} field is required for ${folderName} ${methodName} method, received ${fieldValue}`;

const buildKeyConditionsTypeRequiredErrorMessage = (fieldValue) =>
  `The type field is required for KeyConditions object on list method, received ${fieldValue}`;

module.exports = {
  buildRequiredErrorMessage,
  buildKeyConditionsTypeRequiredErrorMessage
};
