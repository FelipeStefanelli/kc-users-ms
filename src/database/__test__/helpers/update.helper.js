const validContracts =  [
  "16b50910-5cad-5f41-a46d-f1a8462f2b00",
  "ea180c6b-41be-50ee-ba14-4cf34b5f1be5"
];

const validNotifications = [{
  "reservations_alert": 'teste',
  "ride_alert": 'teste'
}];

const validAddress = [{
  "first_line": "teste",
  "complement": "teste",
  "city": "teste",
  "state": "teste",
  "country": "teste",
  "zip_code": "teste"
}]

module.exports = { validContracts, validNotifications, validAddress };
