const { createUser } = require('../../database/create');
const { listByContract } = require('../../database/listByContract');

const TestHelper = require('../../../config/tests/helper');
const {
  buildKeyConditionsOwnerRequiredErrorMessage
} = require('./helpers/messages.helper');
const { parseParams } = require('../../utils/parser');

const createdUsers = [];

const addUser = async (contract) => {
  const { user } = TestHelper.validUser;
  const newUser = { ...user };
  newUser.contracts = contract;
  const { data } = await createUser(newUser);
  createdUsers.push(data);
};

describe('Data base get action test suit', () => {
  beforeAll(async () => {
    await Promise.all([
      addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
      addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
      addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
      addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
      addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
      addUser(['ea180c6b-41be-50ee-ba14-4cf34b5f1be5']),
      addUser(['ea180c6b-41be-50ee-ba14-4cf34b5f1be5']),
      addUser(['ea180c6b-41be-50ee-ba14-4cf34b5f1be5']),
      addUser(['ea180c6b-41be-50ee-ba14-4cf34b5f1be5'])
    ]);
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when getting a user with getByUuid method', () => {
    it('should return user data', async () => {
      const { keyConditionParams } = parseParams({ 'contract@EQ': 'contract_16b50910-5cad-5f41-a46d-f1a8462f2b00' });
      const { data, error } = await listByContract(keyConditionParams, {}, {});

      const { users } = data;
      console.log(users)
      expect(users).toHaveLength(5);
      expect(error).toBeFalsy();

      expect(users[0].contracts).toStrictEqual(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      expect(users[1].contracts).toStrictEqual(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      expect(users[2].contracts).toStrictEqual(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      expect(users[3].contracts).toStrictEqual(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      expect(users[4].contracts).toStrictEqual(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
    });
  });

  describe('when calling listByContract method with a database error', () => {
    it('should return an error', async () => {
      const { keyConditionParams } = parseParams({ 'contract@EQ': 'lalala' });

      await TestHelper.deleteTables();

      const { data, errors } = await listByContract(keyConditionParams, {}, {});

      await TestHelper.createTables();

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
    });
  });
});
