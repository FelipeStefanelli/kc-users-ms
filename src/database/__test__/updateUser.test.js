const { createUser } = require('../create');
const { updateUser } = require('../update');
const { getByUuid } = require('../get');
const { validContracts, validNotifications, validAddress } = require('./helpers/update.helper');

const TestHelper = require('../../../config/tests/helper');
const { buildRequiredErrorMessage } = require('./helpers/messages.helper');

let userUuid;
let validUser;

describe('Data base update action test suit', () => {
  beforeEach(async () => {
    const { user } = TestHelper.validUser;
    const { data } = await createUser(user);
    const { uuid } = data;

    validUser = user;
    userUuid = uuid;
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe.each([
    [ 'type', 'user' ],
    [ 'type', 'contract' ],
    [ 'name', 'Felipe' ],
    [ 'name', 'Joao' ],
    [ 'birth_date', '1995-03-19' ],
    [ 'birth_date', '1988-06-07' ],
    [ 'identifier', '1324569811' ],
    [ 'identifier', 'lalalala' ],
    [ 'identifier_type', 'cpf' ],
    [ 'identifier_type', 'passaporte' ],
    [ 'password', 'M2kdaai2djaUHS4Adsa' ],
    [ 'password', 'uLoisbc14grs3' ],
    [ 'contracts', validContracts ],
    [ 'address', validAddress ],
    [ 'address_verified', null ],
    [ 'address_verification_photo', 'url' ],
    [ 'address_verification_photo', 'http://xxxx.com.br' ],
    [ 'phone', '+5599123456789' ],
    [ 'phone', '+5599573482489' ],
    [ 'phone_verified', false ],
    [ 'email', 'xxxxx' ],
    [ 'email', 'eva@brewer.com.br' ],
    [ 'email_verified', true ],
    [ 'first_login', false ],
    [ 'first_login', true ],
    [ 'welcome_email_sent', false ],
    [ 'welcome_email_sent', true ],
    [ 'notifications', validNotifications ],
    [ 'photo', 'url' ],
    [ 'photo', 'http://lalalal.com' ],
    [ 'driver_license_number', '1234567890' ],
    [ 'driver_license_number', '1287692018' ],
    [ 'driver_license_register_number', '1234567890' ],
    [ 'driver_license_register_number', '1287692018' ],
    [ 'driver_license_first_habilitation_at', '2020-12-30' ],
    [ 'driver_license_expires_at', '2020-12-30' ],
    [ 'driver_license_category', 'A' ],
    [ 'driver_license_category', 'ABCD' ],
    [ 'driver_license_photo', 'url' ],
    [ 'driver_license_photo', 'http:yrl.com.br' ],
    [ 'blocked', false ],
    [ 'blocked', true ],
    [ 'blocked_by', 'user_1d7f2955-a910-4835-b476-b34cce9e224c' ],
    [ 'block_reason', 'laranja' ],
    [ 'block_reason', 'melancia' ],
  ])('when passing %s as %s to updateUser', (fieldName, fieldValue) => {
    it('should return updated user data', async () => {
      const newUser = { ...validUser };
      newUser[fieldName] = fieldValue;

      delete newUser.uuid;

      const { error } = await updateUser(newUser, { uuid: userUuid });

      const { data } = await getByUuid({ uuid: userUuid });

      expect(error).toBeFalsy();
      expect(data).toMatchObject(newUser);
    });
  });


  describe('when updating a user with updateUser method with a database error', () => {
    it('should return an error', async () => {
      const newUser = { ...validUser };

      await TestHelper.deleteTables();

      const { data, error } = await updateUser(newUser, { uuid: userUuid });

      await TestHelper.createTables();

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
    });
  });

  describe.each([
    [null, buildRequiredErrorMessage('user', null, 'database', 'updateUser')],
    [undefined, buildRequiredErrorMessage('user', undefined, 'database', 'updateUser')],
    ['', buildRequiredErrorMessage('user', '', 'database', 'updateUser')],
  ])('when passing user as %s to updateUser', (user, errorMessage) => {
    it('should return an error', async () => {
      const { data, error } = await updateUser(user, { uuid: userUuid });

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
      expect(error[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    ['uuid', null, buildRequiredErrorMessage('uuid', null, 'database', 'updateUser')],
    ['uuid', undefined, buildRequiredErrorMessage('uuid', undefined, 'database', 'updateUser')],
    ['uuid', '', buildRequiredErrorMessage('uuid', '', 'database', 'updateUser')],
  ])('when passing %s as %s to updateUser', (fieldName, fieldValue, errorMessage) => {
    it('should return an error', async () => {
      const newUser = { ...validUser };
      delete newUser.uuid;

      const params = {
        uuid: userUuid
      };

      params[fieldName] = fieldValue;

      const { data, error } = await updateUser(newUser, params);

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
      expect(error[0].details).toBe(errorMessage);
    });
  });
});
