const { createUser } = require('../../database/create');
const { updateObject } = require('../../database/update');

const TestHelper = require('../../../config/tests/helper');

let userUuid;
let userContracts;

describe('Data base update action test suit', () => {
  beforeEach(async () => {
    const { user } = TestHelper.validUser;
    const { data } = await createUser(user);
    const { uuid } = data;
    const { contracts } = data;
    
    userUuid = uuid;
    userContracts = contracts;
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe.each([['ADD_USER'], ['REMOVE_USER'], ['UPDATE_USER']])(
    'when passing action as %s to updateObject',
    (updateAction) => {
      it('should return true and no errors', async () => {
        const result = await updateObject(userUuid, userContracts, updateAction);

        expect(result.error).toBeFalsy();
        expect(result).toBe(true);
      });
    }
  );
});
