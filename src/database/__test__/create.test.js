const { createUser } = require('../../database/create');

const TestHelper = require('../../../config/tests/helper');

const { validUser } = TestHelper;
const { buildRequiredErrorMessage } = require('./helpers/messages.helper');

describe('Data base actions test suit', () => {
  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when creating an user with createUser method', () => {
    it('should return user data', async () => {
      const newUser = { ...validUser.user };
      const { data, errors } = await createUser(newUser);

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();

      expect(data).toHaveProperty('uuid');
      expect(data).toHaveProperty('type');
      expect(data).toHaveProperty('name');
      expect(data).toHaveProperty('birth_date');
      expect(data).toHaveProperty('identifier');
      expect(data).toHaveProperty('identifier_type');
      expect(data).toHaveProperty('password');
      expect(data).toHaveProperty('contracts');
      expect(data).toHaveProperty('address');
      expect(data).toHaveProperty('address_verified');
      expect(data).toHaveProperty('address_verification_photo');
      expect(data).toHaveProperty('phone');
      expect(data).toHaveProperty('phone_verified');
      expect(data).toHaveProperty('email');
      expect(data).toHaveProperty('email_verified');
      expect(data).toHaveProperty('first_login');
      expect(data).toHaveProperty('welcome_email_sent');
      expect(data).toHaveProperty('notifications');
      expect(data).toHaveProperty('photo');
      expect(data).toHaveProperty('driver_license_number');
      expect(data).toHaveProperty('driver_license_register_number');
      expect(data).toHaveProperty('driver_license_first_habilitation_at');
      expect(data).toHaveProperty('driver_license_expires_at');
      expect(data).toHaveProperty('driver_license_category');
      expect(data).toHaveProperty('driver_license_photo');
      expect(data).toHaveProperty('blocked');
      expect(data).toHaveProperty('blocked_by');
      expect(data).toHaveProperty('block_reason');
    });
  });

  describe.each([
    [
      'contracts',
      '',
      buildRequiredErrorMessage('contracts', '', 'database', 'createUser'),
    ],
    [
      'contracts',
      null,
      buildRequiredErrorMessage('contracts', null, 'database', 'createUser'),
    ],
    [
      'contracts',
      undefined,
      buildRequiredErrorMessage('contracts', undefined, 'database', 'createUser'),
    ],
    ['type', '', buildRequiredErrorMessage('type', '', 'database', 'createUser')],
    ['type', null, buildRequiredErrorMessage('type', null, 'database', 'createUser')],
    ['type', undefined, buildRequiredErrorMessage('type', undefined, 'database', 'createUser')],
    ['name', '', buildRequiredErrorMessage('name', '', 'database', 'createUser')],
    ['name', null, buildRequiredErrorMessage('name', null, 'database', 'createUser')],
    ['name', undefined, buildRequiredErrorMessage('name', undefined, 'database', 'createUser')],
    ['birth_date', '', buildRequiredErrorMessage('birth_date', '', 'database', 'createUser')],
    ['birth_date', null, buildRequiredErrorMessage('birth_date', null, 'database', 'createUser')],
    ['birth_date', undefined, buildRequiredErrorMessage('birth_date', undefined, 'database', 'createUser')],
    ['identifier', '', buildRequiredErrorMessage('identifier', '', 'database', 'createUser')],
    ['identifier', null, buildRequiredErrorMessage('identifier', null, 'database', 'createUser')],
    ['identifier', undefined, buildRequiredErrorMessage('identifier', undefined, 'database', 'createUser')],
    ['identifier_type', '', buildRequiredErrorMessage('identifier_type', '', 'database', 'createUser')],
    ['identifier_type', null, buildRequiredErrorMessage('identifier_type', null, 'database', 'createUser')],
    ['identifier_type', undefined, buildRequiredErrorMessage('identifier_type', undefined, 'database', 'createUser')],
    ['password', '', buildRequiredErrorMessage('password', '', 'database', 'createUser')],
    ['password', null, buildRequiredErrorMessage('password', null, 'database', 'createUser')],
    ['password', undefined, buildRequiredErrorMessage('password', undefined, 'database', 'createUser')],
    ['contracts', [], buildRequiredErrorMessage('contracts', '', 'database', 'createUser')],
    ['contracts', null, buildRequiredErrorMessage('contracts', null, 'database', 'createUser')],
    ['contracts', undefined, buildRequiredErrorMessage('contracts', undefined, 'database', 'createUser')],
    ['address', [], buildRequiredErrorMessage('address', '', 'database', 'createUser')],
    ['address', null, buildRequiredErrorMessage('address', null, 'database', 'createUser')],
    ['address', undefined, buildRequiredErrorMessage('address', undefined, 'database', 'createUser')],
    ['address_verified', '', buildRequiredErrorMessage('address_verified', '', 'database', 'createUser')],
    ['address_verified', null, buildRequiredErrorMessage('address_verified', null, 'database', 'createUser')],
    ['address_verified', undefined, buildRequiredErrorMessage('address_verified', undefined, 'database', 'createUser')],
    ['address_verification_photo', '', buildRequiredErrorMessage('address_verification_photo', '', 'database', 'createUser')],
    ['address_verification_photo', null, buildRequiredErrorMessage('address_verification_photo', null, 'database', 'createUser')],
    ['address_verification_photo', undefined, buildRequiredErrorMessage('address_verification_photo', undefined, 'database', 'createUser')],
    ['phone', '', buildRequiredErrorMessage('phone', '', 'database', 'createUser')],
    ['phone', null, buildRequiredErrorMessage('phone', null, 'database', 'createUser')],
    ['phone', undefined, buildRequiredErrorMessage('phone', undefined, 'database', 'createUser')],
    ['phone_verified', '', buildRequiredErrorMessage('phone_verified', '', 'database', 'createUser')],
    ['phone_verified', null, buildRequiredErrorMessage('phone_verified', null, 'database', 'createUser')],
    ['phone_verified', undefined, buildRequiredErrorMessage('phone_verified', undefined, 'database', 'createUser')],
    ['email', '', buildRequiredErrorMessage('email', '', 'database', 'createUser')],
    ['email', null, buildRequiredErrorMessage('email', null, 'database', 'createUser')],
    ['email', undefined, buildRequiredErrorMessage('email', undefined, 'database', 'createUser')],
    ['email_verified', '', buildRequiredErrorMessage('email_verified', '', 'database', 'createUser')],
    ['email_verified', null, buildRequiredErrorMessage('email_verified', null, 'database', 'createUser')],
    ['email_verified', undefined, buildRequiredErrorMessage('email_verified', undefined, 'database', 'createUser')],
    ['first_login', '', buildRequiredErrorMessage('first_login', '', 'database', 'createUser')],
    ['first_login', null, buildRequiredErrorMessage('first_login', null, 'database', 'createUser')],
    ['first_login', undefined, buildRequiredErrorMessage('first_login', undefined, 'database', 'createUser')],
    ['welcome_email_sent', '', buildRequiredErrorMessage('welcome_email_sent', '', 'database', 'createUser')],
    ['welcome_email_sent', null, buildRequiredErrorMessage('welcome_email_sent', null, 'database', 'createUser')],
    ['welcome_email_sent', undefined, buildRequiredErrorMessage('welcome_email_sent', undefined, 'database', 'createUser')],
    ['notifications', [], buildRequiredErrorMessage('notifications', '', 'database', 'createUser')],
    ['notifications', null, buildRequiredErrorMessage('notifications', null, 'database', 'createUser')],
    ['notifications', undefined, buildRequiredErrorMessage('notifications', undefined, 'database', 'createUser')],
    ['photo', '', buildRequiredErrorMessage('photo', '', 'database', 'createUser')],
    ['photo', null, buildRequiredErrorMessage('photo', null, 'database', 'createUser')],
    ['photo', undefined, buildRequiredErrorMessage('photo', undefined, 'database', 'createUser')],
    ['driver_license_number', '', buildRequiredErrorMessage('driver_license_number', '', 'database', 'createUser')],
    ['driver_license_number', null, buildRequiredErrorMessage('driver_license_number', null, 'database', 'createUser')],
    ['driver_license_number', undefined, buildRequiredErrorMessage('driver_license_number', undefined, 'database', 'createUser')],
    ['driver_license_register_number', '', buildRequiredErrorMessage('driver_license_register_number', '', 'database', 'createUser')],
    ['driver_license_register_number', null, buildRequiredErrorMessage('driver_license_register_number', null, 'database', 'createUser')],
    ['driver_license_register_number', undefined, buildRequiredErrorMessage('driver_license_register_number', undefined, 'database', 'createUser')],
    ['driver_license_first_habilitation_at', '', buildRequiredErrorMessage('driver_license_first_habilitation_at', '', 'database', 'createUser')],
    ['driver_license_first_habilitation_at', null, buildRequiredErrorMessage('driver_license_first_habilitation_at', null, 'database', 'createUser')],
    ['driver_license_first_habilitation_at', undefined, buildRequiredErrorMessage('driver_license_first_habilitation_at', undefined, 'database', 'createUser')],
    ['driver_license_expires_at', '', buildRequiredErrorMessage('driver_license_expires_at', '', 'database', 'createUser')],
    ['driver_license_expires_at', null, buildRequiredErrorMessage('driver_license_expires_at', null, 'database', 'createUser')],
    ['driver_license_expires_at', undefined, buildRequiredErrorMessage('driver_license_expires_at', undefined, 'database', 'createUser')],
    ['driver_license_category', '', buildRequiredErrorMessage('driver_license_category', '', 'database', 'createUser')],
    ['driver_license_category', null, buildRequiredErrorMessage('driver_license_category', null, 'database', 'createUser')],
    ['driver_license_category', undefined, buildRequiredErrorMessage('driver_license_category', undefined, 'database', 'createUser')],
    ['driver_license_photo', '', buildRequiredErrorMessage('driver_license_photo', '', 'database', 'createUser')],
    ['driver_license_photo', null, buildRequiredErrorMessage('driver_license_photo', null, 'database', 'createUser')],
    ['driver_license_photo', undefined, buildRequiredErrorMessage('driver_license_photo', undefined, 'database', 'createUser')],
    ['blocked', '', buildRequiredErrorMessage('blocked', '', 'database', 'createUser')],
    ['blocked', null, buildRequiredErrorMessage('blocked', null, 'database', 'createUser')],
    ['blocked', undefined, buildRequiredErrorMessage('blocked', undefined, 'database', 'createUser')],
    ['blocked_by', '', buildRequiredErrorMessage('blocked_by', '', 'database', 'createUser')],
    ['blocked_by', null, buildRequiredErrorMessage('blocked_by', null, 'database', 'createUser')],
    ['blocked_by', undefined, buildRequiredErrorMessage('blocked_by', undefined, 'database', 'createUser')],
    ['block_reason', '', buildRequiredErrorMessage('block_reason', '', 'database', 'createUser')],
    ['block_reason', null, buildRequiredErrorMessage('block_reason', null, 'database', 'createUser')],
    ['block_reason', undefined, buildRequiredErrorMessage('block_reason', undefined, 'database', 'createUser')]
  ])('when passing %s as %s to createUser', (fieldName, fieldValue, errorMessage) => {
    it('should return an error', async () => {
      const newUser = { ...validUser.user };
      newUser[fieldName] = fieldValue;
      console.log(newUser);
      

      const { data, error } = await createUser(newUser);

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
      expect(error[0].details).toBe(errorMessage);
    });
  });

  describe('when creating an user with createUser method with a database error', () => {
    it('should return an error', async () => {
      const newUser = { ...validUser.user };

      await TestHelper.deleteTables();

      const { data, error } = await createUser(newUser);

      await TestHelper.createTables();

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
    });
  });

  describe.each([
    [null, buildRequiredErrorMessage('user', null, 'database', 'createUser')],
    [undefined, buildRequiredErrorMessage('user', undefined, 'database', 'createUser')],
  ])('when passing user as %s to createUser', (user, errorMessage) => {
    it('should return an error', async () => {
      const { data, error } = await createUser(user);

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
      expect(error[0].details).toBe(errorMessage);
    });
  });
});
