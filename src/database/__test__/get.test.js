const { createUser } = require('../../database/create');
const { getByUuid, batchGetUsers } = require('../../database/get');

const TestHelper = require('../../../config/tests/helper');
const { buildRequiredErrorMessage } = require('./helpers/messages.helper');

let userUuid;
let createdUser;

describe('Data base get action test suit', () => {
  beforeEach(async () => {
    const { user } = TestHelper.validUser;
    const { data } = await createUser(user);
    const { uuid } = data;

    createdUser = data;
    userUuid = uuid;
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when getting a user with getByUuid method', () => {
    it('should return user data', async () => {
      const { data, error } = await getByUuid({ uuid: userUuid });

      expect(error).toBeFalsy();
      expect(data).toBeTruthy();

      expect(data).toStrictEqual(createdUser);
    });
  });

  describe('when getting a non existing user with a wrong uuid with getByUuid method', () => {
    it('should return user not found error', async () => {
      const { data, error } = await getByUuid({ uuid: '1' });

      expect(data).toBeFalsy();
      expect(error).toBeFalsy();
    });
  });

  describe('when trying to call batchGetusers method with a database error', () => {
    it('should return user not found error', async () => {
      const { data, error } = await batchGetUsers('invalid params');

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
    });
  });

  describe.each([
    ['', buildRequiredErrorMessage('uuid', '', 'database', 'getByUuid')],
    [null, buildRequiredErrorMessage('uuid', null, 'database', 'getByUuid')],
    [undefined, buildRequiredErrorMessage('uuid', undefined, 'database', 'getByUuid')],
  ])('when passing uuid as %s to getByUuid', (userUuid, errorMessage) => {
    it('should return an error', async () => {
      const newUser = {
        uuid: userUuid
      };

      const { data, error } = await getByUuid(newUser);

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
      expect(error[0].details).toBe(errorMessage);
    });
  });

  describe('when getting a user with getByUuid method with database error', () => {
    it('should return an error', async () => {
      await TestHelper.deleteTables();

      const { data, error } = await getByUuid({ uuid: userUuid });

      await TestHelper.createTables();

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
    });
  });
});
