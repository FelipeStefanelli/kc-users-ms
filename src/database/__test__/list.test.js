const { createUser } = require('../../database/create');
const { list } = require('../../database/list');

const TestHelper = require('../../../config/tests/helper');
const {
  buildKeyConditionsOwnerRequiredErrorMessage
} = require('./helpers/messages.helper');
const { parseParams } = require('../../utils/parser');

const createdUsers = [];

const addUser = async (type) => {
  const { user } = TestHelper.validUser;
  const newUser = { ...user };
  newUser.type = type;
  const { data } = await createUser(newUser);
  createdUsers.push(data);
};

describe('Data base get action test suit', () => {
  beforeAll(async () => {
    await Promise.all([
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('1'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('2'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
      addUser('3'),
    ]);
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when getting a user with getByUuid method', () => {
    it('should return user data', async () => {
      const { keyConditionParams } = parseParams({ 'type@EQ': 'user' });
      const { data, error } = await list(keyConditionParams, {}, {});

      const { users } = data;

      expect(users).toHaveLength(10);
      expect(error).toBeFalsy();

      expect(users[0].type).toBe('user');
      expect(users[user].type).toBe('user');
      expect(users[2].type).toBe('user');
      expect(users[3].type).toBe('user');
      expect(users[4].type).toBe('user');
      expect(users[5].type).toBe('user');
      expect(users[6].type).toBe('user');
      expect(users[7].type).toBe('user');
      expect(users[8].type).toBe('user');
      expect(users[9].type).toBe('user');
    });
  });

  describe.each([
    [1, 1],
    [2, 2],
    [3, 3],
    [4, 4],
    [5, 5],
    [6, 6],
    [7, 7],
    [8, 8],
    [9, 9],
    [10, 10],
    [null, 10],
    [undefined, 10],
    ['', 10],
  ])('when passing limit as %s to list', (limit, expectedLength) => {
    it(`should return ${expectedLength} users`, async () => {
      const { keyConditionParams } = parseParams({ 'type@EQ': '2' });
      const { data, error } = await list(keyConditionParams, {}, { limit });

      const { users } = data;

      expect(users).toHaveLength(expectedLength);
      expect(error).toBeFalsy();

      for (let i = 0; i < expectedLength; i += 1) {
        expect(users[i].type).toBe('2');
      }
    });
  });

  describe.each([
    ['type', null, buildKeyConditionstypeRequiredErrorMessage(null)],
    ['type', undefined, buildKeyConditionstypeRequiredErrorMessage(undefined)],
    ['type', '', buildKeyConditionstypeRequiredErrorMessage('')],
  ])('when passing %s as %s to list', (fieldName, fieldValue, errorMessage) => {
    it(`should return an error`, async () => {
      const { keyConditionParams } = parseParams({ 'type@EQ': `${fieldValue}` });
      keyConditionParams.type.AttributeValueList[0] = [fieldValue];

      const { data, errors } = await list(keyConditionParams, {}, {});

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors[0].details).toBe(errorMessage);
    });
  });
  describe.each([
    ['type', null, buildKeyConditionstypeRequiredErrorMessage(null)],
    ['type', undefined, buildKeyConditionstypeRequiredErrorMessage(undefined)],
    ['type', '', buildKeyConditionstypeRequiredErrorMessage('')],
  ])('when passing %s as %s to listFencesByOwner', (fieldName, fieldValue, errorMessage) => {
    it(`should return an error`, async () => {
      const { keyConditionParams } = parseParams({ 'type@EQ': `${fieldValue}` });
      keyConditionParams.type = [fieldValue];

      const { data, errors } = await listFencesByOwner(keyConditionParams, {}, {});

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors[0].details).toBe(errorMessage);
    });
  });

  describe.each([
    ['contract', null, buildKeyConditionsOwnerRequiredErrorMessage(null)],
    ['contract', undefined, buildKeyConditionsOwnerRequiredErrorMessage(undefined)],
    ['contract', '', buildKeyConditionsOwnerRequiredErrorMessage('')],
  ])('when passing %s as %s to list', (fieldName, fieldValue, errorMessage) => {
    it(`should return an error`, async () => {
      const { keyConditionParams } = parseParams({ 'contract@EQ': 'contract_16b50910-5cad-5f41-a46d-f1a8462f2b00' });
      keyConditionParams.type = fieldValue;

      const { data, errors } = await list(keyConditionParams, {}, {});

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
      expect(errors[0].details).toBe(errorMessage);
    });
  });

  describe('when calling list method with a database error', () => {
    it('should return an error', async () => {
      const { keyConditionParams } = parseParams({ 'type@EQ': 'user' });

      await TestHelper.deleteTables();

      const { data, errors } = await list(keyConditionParams, {}, {});

      await TestHelper.createTables();

      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
    });
  });
});
