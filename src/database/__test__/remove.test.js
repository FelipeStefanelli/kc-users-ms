const { createUser } = require('../../database/create');
const { removeUser } = require('../../database/remove');

const TestHelper = require('../../../config/tests/helper');
const { buildRequiredErrorMessage } = require('./helpers/messages.helper');

let userUuid;

describe('Data base remove action test suit', () => {
  beforeEach(async () => {
    const { user } = TestHelper.validUser;
    const { data } = await createUser(user);
    const { uuid } = data;

    userUuid = uuid;
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when removing a user with removeUser method', () => {
    it('should return true', async () => {
      const { data, error } = await removeUser({ uuid: userUuid });

      expect(error).toBeFalsy();

      expect(data).toBe(true);
    });
  });

  describe('when removing a non existing user with invalid uuid with removeUser method', () => {
    it('should return an error', async () => {
      const { data, error } = await removeUser({ uuid: '1' });

      expect(error).toBeTruthy();
      expect(data).toBeFalsy();
    });
  });

  describe.each([
    ['uuid', '', buildRequiredErrorMessage('uuid', '', 'database', 'removeUser')],
    ['uuid', null, buildRequiredErrorMessage('uuid', null, 'database', 'removeUser')],
    ['uuid', undefined, buildRequiredErrorMessage('uuid', undefined, 'database', 'removeUser')],
  ])('when passing %s as %s to removeUser method', (fieldName, fieldValue, errorMessage) => {
    it('should return an error', async () => {
      const newUser = {
        uuid: userUuid
      };

      newUser[fieldName] = fieldValue;

      const { data, error } = await removeUser(newUser);

      expect(data).toBeFalsy();
      expect(error).toBeTruthy();
      expect(error[0].details).toBe(errorMessage);
    });
  });

  describe('when removing an already removed user with removeUser method', () => {
    it('should return an error', async () => {
      await removeUser({ uuid: userUuid });

      const { data, error } = await removeUser({ uuid: userUuid });

      expect(error).toBeTruthy();
      expect(data).toBeFalsy();
    });
  });
});
