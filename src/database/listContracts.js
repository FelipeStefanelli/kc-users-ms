const { tableName, dynamoDb } = require('../../config/dynamodb');
const { KEY_CONDITIONS_TYPE_REQUIRED } = require('../constants/errors');

const listContracts = async (
  { type }
) => {

  if (!type) {
    return { errors: KEY_CONDITIONS_TYPE_REQUIRED(type) };
  }

  const typeValue = type.AttributeValueList[0];

  if (!typeValue || typeValue === 'null' || typeValue === 'undefined') {
    return { errors: KEY_CONDITIONS_TYPE_REQUIRED(typeValue) };
  }

  const params = {
    TableName: tableName,
    IndexName: "typeIndex",
    KeyConditions: {
      type,
      uuid: {
        ComparisonOperator: 'BEGINS_WITH',
        AttributeValueList: ['contract'],
      },
    },
  };

  const data = await dynamoDb
    .query(params)
    .promise()
    .catch((error) => {
      console.error(error);
      return { error: error.message };
    });

  if (data.error) {
    return { errors: [{ details: data.error }] };
  }

  const { Items, LastEvaluatedKey } = data;

  return {
    data: {
      users: Items,
      last_evaluated_key: LastEvaluatedKey,
    },
  };
};

module.exports = {
    listContracts
};
