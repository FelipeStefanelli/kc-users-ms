const { tableName, dynamoDb } = require('../../config/dynamodb');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');
const { buildUpdateParams } = require('../utils/parser');

const { getByUuid } = require('./get');

const updateObject = async (userUuid, contractIdentifier, updateAction) =>
  new Promise(async (resolve, reject) => {
    if (!userUuid) {
      return REQUEST_FIELD_REQUIRED('userUuid', 'updateObject', 'database', userUuid);
    }
    if (!contractIdentifier) {
      return REQUEST_FIELD_REQUIRED('contractIdentifier', 'updateObject', 'database', contractIdentifier);
    }
    if (!updateAction) {
      return REQUEST_FIELD_REQUIRED('updateAction', 'updateObject', 'database', updateAction);
    }

    const objectUuid = `contract_${contractIdentifier}`;

    const now = new Date().toISOString();

    var updateExpresion;
    let expressionAttributeValues = {};

    if (updateAction === 'REMOVE_USER') {
      const object = await getByUuid({ uuid: objectUuid });

      const { users_identifiers } = object.data;
      const userIndex = users_identifiers.indexOf(userUuid);

      if (userIndex === -1) {
        return true;
      }

      updateExpresion = `REMOVE users_identifiers[${userIndex}] SET`;
    }

    if (updateAction === 'ADD_USER') {
      updateExpresion = `SET users_identifiers = list_append(if_not_exists(users_identifiers, :empty_list), :users), `;

      expressionAttributeValues = {
        ':users': [`user_${userUuid}`],
        ':empty_list': [],
      };
    }

    if (updateAction === 'UPDATE_USER') {
      updateExpresion = `SET users_identifiers = list_append(if_not_exists(users_identifiers, :empty_list), :users),`;

      expressionAttributeValues = {
        ':users': [userUuid],
        ':empty_list': [],
      };
    }

    const params = {
      TableName: tableName,
      Key: {
        uuid: objectUuid
      },
      ExpressionAttributeNames: {
        "#type": "type"
      },
      UpdateExpression: `${updateExpresion} activated_at = if_not_exists(activated_at, :activated_at), updated_at = :updated_at, #type = :type`,
      ExpressionAttributeValues: {
        ...expressionAttributeValues,
        ':activated_at': now,
        ':updated_at': now,
        ':type': 'contract'
      },
      ReturnValues: 'UPDATED_NEW',
    };

    const data = await dynamoDb
      .update(params)
      .promise()
      .catch((error) => {
        console.error(error);
        return { error: error.message };
      });

    if (data.error) {
      return reject({ error: [{ details: `Object ${contractIdentifier} could not be updated` }] });
    }

    return resolve();
  });

const updateUser = async (user, { uuid }) => {
  if (!user) {
    return { error: REQUEST_FIELD_REQUIRED('user', 'updateUser', 'database', user) };
  }

  if (!uuid) {
    return { error: REQUEST_FIELD_REQUIRED('uuid', 'updateUser', 'database', uuid) };
  }

  if (user.contracts && user.contracts.length <= 0) {
    return {
      error: REQUEST_FIELD_REQUIRED(
        'contracts',
        'updateUser',
        'database',
        user.contracts
      ),
    };
  }
  const now = new Date().toISOString();

  const newUser = {
    ...user,
    updated_at: now,
  };

  const updateParams = buildUpdateParams(newUser);

  const params = {
    TableName: tableName,
    Key: {
      uuid
    },
    Expected: {
      uuid: {
        Exists: true,
        Value: uuid,
      }
    },
    AttributeUpdates: updateParams,
    ReturnValues: 'UPDATED_OLD',
  };

  const data = await dynamoDb
    .update(params)
    .promise()
    .catch((error) => {
      console.error(error);
      return { error: error.message };
    });

  if (data.error) {
    return { error: [{ details: data.error }] };
  }

  const updatedItemAttributes = data.Attributes;

  if (updatedItemAttributes.contracts) {
    const promises = [];

    const oldContracts = updatedItemAttributes.contracts;
    const newContracts = user.contracts;

    const excludedItems = oldContracts.filter(
      (contract) => !newContracts.includes(contract)
    );
    const addedItems = newContracts.filter(
      (contract) => !oldContracts.includes(contract)
    );

    if (excludedItems.length > 0) {
      excludedItems.forEach((contract) => {
        promises.push(updateObject(uuid, contract, 'REMOVE_USER'));
      });
    }

    if (addedItems.length > 0) {
      addedItems.forEach((contract) => {
        promises.push(updateObject(uuid, contract, 'UPDATE_USER'));
      });
    }

    await Promise.all(promises);
  }

  return { data: data.Attributes };
};

module.exports = {
  updateUser,
  updateObject,
};
