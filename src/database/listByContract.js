const { tableName, dynamoDb } = require('../../config/dynamodb');
const { KEY_CONDITIONS_CONTRACT_REQUIRED } = require('../constants/errors');

const listByContract = async (
    { contract }
) => {
    if (!contract) {
        return { errors: KEY_CONDITIONS_CONTRACT_REQUIRED(contract) };
    }

    const contractValue = contract.AttributeValueList[0];

    if (!contractValue || contractValue === 'null' || contractValue === 'undefined') {
        return { errors: KEY_CONDITIONS_CONTRACT_REQUIRED(contractValue) };
    }

    const getParams = {
        TableName: tableName,
        Key: {
            uuid: contract.AttributeValueList[0]
        },
    };

    const queryData = await dynamoDb
        .get(getParams)
        .promise()
        .catch((error) => {
            console.error(error);
            return { error: error.message };
        });

    if (queryData.error) {
        return { errors: [{ details: queryData.error }] };
    }

    const { Item } = queryData;

    const contractUsers = Item.users_identifiers;

    const batchParams = {
        RequestItems: {
            [tableName]: {
                Keys: contractUsers.map((user) => {
                    return { uuid: user };
                }),
            }
        }
    };

    const batchData = await dynamoDb
        .batchGet(batchParams)
        .promise()
        .catch((error) => {
            console.error(error);
            return { error: error.message };
        });

    const { Responses, LastEvaluatedKey } = batchData;
    return {
        data: {
            users: Responses[tableName],
            last_evaluated_key: LastEvaluatedKey,
        },
    };
};

module.exports = {
    listByContract,
};
