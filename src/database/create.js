const uuidv4 = require('uuid/v4');
const { tableName, dynamoDb } = require('../../config/dynamodb');
const { REQUEST_FIELD_REQUIRED, FORM_FIELD_REQUIRED } = require('../constants/errors');

const { updateObject } = require('./update');

const createUser = async (user) => {
  if (!user) {
    return { error: REQUEST_FIELD_REQUIRED('user', 'createUser', 'database', user) };
  }

  const {
    type,
    name,
    birth_date,
    identifier,
    identifier_type,
    password,
    contracts,
    address,
    address_verified,
    address_verification_photo,
    phone,
    phone_verified,
    email,
    email_verified,
    first_login,
    welcome_email_sent,
    notifications,
    photo,
    driver_license_number,
    driver_license_register_number,
    driver_license_first_habilitation_at,
    driver_license_expires_at,
    driver_license_category,
    driver_license_photo,
    users_identifiers,
    registered_at,
    blocked,
    blocked_by,
    block_reason
  } = user;

  if (!type) {
    return { error: FORM_FIELD_REQUIRED('type', 'createUser', 'database', type) };
  }
  if (!name) {
    return { error: FORM_FIELD_REQUIRED('name', 'createUser', 'database', name) };
  }
  if (!birth_date) {
    return {
      error: FORM_FIELD_REQUIRED('birth_date', 'createUser', 'database', birth_date),
    };
  }
  if (!identifier) {
    return { error: FORM_FIELD_REQUIRED('identifier', 'createUser', 'database', identifier) };
  }
  if (!identifier_type) {
    return { error: FORM_FIELD_REQUIRED('identifier_type', 'createUser', 'database', identifier_type) };
  }
  if (!password) {
    return { error: FORM_FIELD_REQUIRED('password', 'createUser', 'database', password) };
  }
  if (!contracts || (contracts && contracts.length === 0)) {
    return { error: FORM_FIELD_REQUIRED('contracts', 'createUser', 'database', contracts) };
  }
  if (!address || (address && address.length === 0)) {
    return { error: FORM_FIELD_REQUIRED('address', 'createUser', 'database', address) };
  }
  if (address_verified === "" || address_verified === null || address_verified === undefined) {
    return { error: FORM_FIELD_REQUIRED('address_verified', 'createUser', 'database', address_verified) };
  }
  if (!address_verification_photo) {
    return { error: FORM_FIELD_REQUIRED('address_verification_photo', 'createUser', 'database', address_verification_photo) };
  }
  if (!phone) {
    return { error: FORM_FIELD_REQUIRED('phone', 'createUser', 'database', phone) };
  }
  if (phone_verified === "" || phone_verified === null || phone_verified === undefined) {
    return { error: FORM_FIELD_REQUIRED('phone_verified', 'createUser', 'database', phone_verified) };
  }
  if (!email) {
    return { error: FORM_FIELD_REQUIRED('email', 'createUser', 'database', email) };
  }
  if (email_verified === "" || email_verified === null || email_verified === undefined) {
    return { error: FORM_FIELD_REQUIRED('email_verified', 'createUser', 'database', email_verified) };
  }
  if (first_login === "" || first_login === null || first_login === undefined)  {
    return { error: FORM_FIELD_REQUIRED('first_login', 'createUser', 'database', first_login) };
  }
  if (welcome_email_sent === "" || welcome_email_sent === null || welcome_email_sent === undefined) {
    return { error: FORM_FIELD_REQUIRED('welcome_email_sent', 'createUser', 'database', welcome_email_sent) };
  }
  if (!notifications || (notifications && notifications.length === 0)) {
    return { error: FORM_FIELD_REQUIRED('notifications', 'createUser', 'database', notifications) };
  }
  if (!photo) {
    return { error: FORM_FIELD_REQUIRED('photo', 'createUser', 'database', photo) };
  }
  if (!driver_license_number) {
    return { error: FORM_FIELD_REQUIRED('driver_license_number', 'createUser', 'database', driver_license_number) };
  }
  if (!driver_license_register_number) {
    return { error: FORM_FIELD_REQUIRED('driver_license_register_number', 'createUser', 'database', driver_license_register_number) };
  }
  if (!driver_license_first_habilitation_at) {
    return { error: FORM_FIELD_REQUIRED('driver_license_first_habilitation_at', 'createUser', 'database', driver_license_first_habilitation_at) };
  }
  if (!driver_license_expires_at) {
    return { error: FORM_FIELD_REQUIRED('driver_license_expires_at', 'createUser', 'database', driver_license_expires_at) };
  }
  if (!driver_license_category) {
    return { error: FORM_FIELD_REQUIRED('driver_license_category', 'createUser', 'database', driver_license_category) };
  }
  if (!driver_license_photo) {
    return { error: FORM_FIELD_REQUIRED('driver_license_photo', 'createUser', 'database', driver_license_photo) };
  }
  if (blocked === "" || blocked === null || blocked === undefined) {
    return { error: FORM_FIELD_REQUIRED('blocked', 'createUser', 'database', blocked) };
  }
  if (!blocked_by) {
    return { error: FORM_FIELD_REQUIRED('blocked_by', 'createUser', 'database', blocked_by) };
  }
  if (!block_reason) {
    return { error: FORM_FIELD_REQUIRED('block_reason', 'createUser', 'database', block_reason) };
  }

  const userUuid = uuidv4();
  const now = new Date().toISOString();
  const passwordValue = Buffer.from(password).toString('base64');
  const params = {
    TableName: tableName,
    Item: {
      uuid: `user_${userUuid}`,
      type,
      name,
      birth_date,
      identifier,
      identifier_type,
      password: passwordValue,
      contracts,
      address,
      address_verified,
      address_verification_photo,
      phone,
      phone_verified,
      email,
      email_verified,
      first_login,
      welcome_email_sent,
      notifications,
      photo,
      driver_license_number,
      driver_license_register_number,
      driver_license_first_habilitation_at,
      driver_license_expires_at,
      driver_license_category,
      driver_license_photo,
      users_identifiers,
      registered_at,
      activated_at: now,
      blocked,
      blocked_by,
      block_reason,
      "access-token": '',
      "refresh-token": []
    },
  };

  const promises = [];
  promises.push(
    dynamoDb
      .put(params)
      .promise()
      .catch((error) => console.error(error))
  );

  contracts.forEach((contractIdentifier) => {
    promises.push(updateObject(userUuid, contractIdentifier, 'ADD_USER'));
  });


  await Promise.all(promises);


  return { data: params.Item };
};

module.exports = {
  createUser,
};