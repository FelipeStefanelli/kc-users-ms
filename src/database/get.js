const { tableName, dynamoDb } = require('../../config/dynamodb');
const { REQUEST_FIELD_REQUIRED } = require('../constants/errors');

const getByUuid = async ({ uuid }) => {
  if (!uuid) {
    return { error: REQUEST_FIELD_REQUIRED('uuid', 'getByUuid', 'database', uuid) };
  }

  const params = {
    TableName: tableName,
    Key: {
      uuid
    },
  };

  const data = await dynamoDb
    .get(params)
    .promise()
    .catch((error) => {
      console.error(error);
      return { error: error.message };
    });

  if (data.error) {
    return { error: [{ details: data.error }] };
  }
  return { data: data.Item };
};

const batchGetUsers = async (params) => {
  const data = await dynamoDb
    .batchGet(params)
    .promise()
    .catch((error) => {
      console.error(error);
    });

  return { data: data.Responses[tableName] };
};

module.exports = {
  getByUuid,
  batchGetUsers,
};
