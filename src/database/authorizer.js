const { tableName, dynamoDb } = require('../../config/dynamodb');
const { KEY_CONDITIONS_TOKEN_REQUIRED, KEY_CONDITIONS_APITOKEN_REQUIRED } = require('../constants/errors');
const jwt = require('jsonwebtoken');

const authorizer = async (accessToken, apiToken) => {
    if (!accessToken) {
        return { errors: KEY_CONDITIONS_TOKEN_REQUIRED(accessToken) };
    };
    if (!apiToken) {
        return { errors: KEY_CONDITIONS_APITOKEN_REQUIRED(apiToken) };
    }
    let accessTokenType = accessToken.split(' ')[0];
    let accessTokenValue = accessToken.split(' ')[1];

    if (apiToken === 'abc123456') {
        if (accessTokenType == 'Bearer') {
            let legit = jwt.verify(accessTokenValue, apiToken);
            let email = '';
            if (legit.data) {
                email = {
                    ComparisonOperator: 'EQ',
                    AttributeValueList: [legit.data.email]
                };
            };

            const params = {
                TableName: tableName,
                IndexName: "emailIndex",
                KeyConditions: {
                    email: email,
                    uuid: {
                        ComparisonOperator: 'BEGINS_WITH',
                        AttributeValueList: ['user'],
                    }
                }
            };

            const data = await dynamoDb
                .query(params)
                .promise()
                .catch((error) => {
                    console.error(error);
                    return { error: error.message };
                });

            if (data.error) {
                return { errors: [{ details: data.error }] };
            };

            const { Items } = data;

            return {
                data: {
                    users: Items
                }
            };
        } else if (accessTokenType == 'Basic') {
            const decodedAccessToken = Buffer.from(accessTokenValue, 'base64').toString().split(':');
            return {
                data: {
                    encodedToken: accessTokenValue,
                    public: decodedAccessToken[0],
                    secret: decodedAccessToken[1]
                }
            };
        };
    };
};

module.exports = {
    authorizer
};
