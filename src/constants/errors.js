const { t } = require('../utils/i18n');

const REQUEST_FIELD_REQUIRED = (fieldName, methodName, folder, receivedValue) => {
  return [
    {
      field: fieldName,
      details: `The ${fieldName} field is required for ${folder} ${methodName} method, received ${receivedValue}`,
    },
  ];
};

const FORM_FIELD_REQUIRED = (fieldName, methodName, folder, receivedValue) => {
  return [
    {
      field: fieldName,
      message: t(`validation.fields.${fieldName}.required`),
      details: `The ${fieldName} field is required for ${folder} ${methodName} method, received ${receivedValue}`,
    },
  ];
};

const PAGINATION_KEY_REQUIRED = (keyType, receivedValue) => {
  return [
    {
      field: keyType,
      details: `Both the paginationHash and paginationRange fields are required for listUsers method, received ${receivedValue}`,
    },
  ];
};

const KEY_CONDITIONS_CONTRACT_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'contract',
      details: `The contract field is required for KeyConditions object on listByContract method, received ${receivedValue}`,
    },
  ];
};

const KEY_CONDITIONS_TYPE_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'type',
      details: `The type field is required for KeyConditions object on list method, received ${receivedValue}`,
    },
  ];
};

const KEY_CONDITIONS_EMAIL_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'email',
      details: `The email field is required for KeyConditions object on list method, received ${receivedValue}`,
    },
  ];
};

const KEY_CONDITIONS_PASSWORD_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'password',
      details: `The password field is required for KeyConditions object on list method, received ${receivedValue}`,
    },
  ];
};

const KEY_CONDITIONS_APITOKEN_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'api-token',
      details: `The api token field is required for KeyConditions object on list method, received ${receivedValue}`,
    },
  ];
};

const KEY_CONDITIONS_REFRESHTOKEN_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'refresh-token',
      details: `The refresh token field is required for KeyConditions object on list method, received ${receivedValue}`,
    },
  ];
};
const KEY_CONDITIONS_REFRESHTOKENEXPIRE_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'refresh-token-expire',
      details: `The refresh token expire field is required for KeyConditions object on list method, received ${receivedValue}`,
    },
  ];
};
const KEY_CONDITIONS_TOKEN_REQUIRED = (receivedValue) => {
  return [
    {
      field: 'access-token',
      details: `The access token field is required for KeyConditions object on list method, received ${receivedValue}`,
    },
  ];
};

module.exports = {
  REQUEST_FIELD_REQUIRED,
  FORM_FIELD_REQUIRED,
  PAGINATION_KEY_REQUIRED,
  KEY_CONDITIONS_CONTRACT_REQUIRED,
  KEY_CONDITIONS_TYPE_REQUIRED,
  KEY_CONDITIONS_EMAIL_REQUIRED,
  KEY_CONDITIONS_TOKEN_REQUIRED,
  KEY_CONDITIONS_PASSWORD_REQUIRED,
  KEY_CONDITIONS_APITOKEN_REQUIRED,
  KEY_CONDITIONS_REFRESHTOKEN_REQUIRED,
  KEY_CONDITIONS_REFRESHTOKENEXPIRE_REQUIRED
};
