FROM node:8

WORKDIR /usr/src/app

copy package.json ./
RUN npm install

COPY . .
RUN npm test
