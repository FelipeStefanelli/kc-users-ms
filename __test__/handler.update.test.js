const { update } = require('../handler');
const { createUser } = require('../src/database/create');
const { i18n } = require('../src/utils/i18n');
const Response = require('../src/utils/response');
const TestHelper = require('../config/tests/helper');
const { buildRequiredErrorMessage } = require('../src/database/__test__/helpers/messages.helper');

const {
    emptyType,
    requiredType,
    emptyName,
    requiredName,
    emptyBirth_date,
    requiredBirth_date,
    emptyIdentifier,
    requiredIdentifier,
    emptyIdentifierType,
    requiredIdentifierType,
    emptyPassword,
    requiredPassword,
    emptyContracts,
    requiredContracts,
    emptyAdress,
    requiredAdress,
    requiredAdressVerified,
    emptyAdressVerificationPhoto,
    requiredAdressVerificationPhoto,
    emptyPhone,
    requiredPhone,
    requiredPhoneVerified,
    emptyEmail,
    requiredEmail,
    requiredEmailVerified,
    requiredFirstLogin,
    requiredWelcomeEmailSent,
    emptyNotifications,
    requiredNotifications,
    emptyPhoto,
    requiredPhoto,
    emptyDriverLicenseNumber,
    requiredDriverLicenseNumber,
    emptyDriverLicenseRegisterNumber,
    requiredDriverLicenseRegisterNumber,
    emptyDriverLicenseFirstHabilitationAt,
    requiredDriverLicenseFirstHabilitationAt,
    emptyDriverLicenseExpiresAt,
    requiredDriverLicenseExpiresAt,
    emptyDriverLicenseCategory,
    requiredDriverLicenseCategory,
    emptyDriveLicensePhoto,
    requiredDriverLicensePhoto,
    requiredBlocked,
    emptyBlockedBy,
    requiredBlockedBy,
    emptyBlockedReason,
    requiredBlockedReason
} = TestHelper.validationErrors;

const { BAD_REQUEST } = Response.httpStatusCodes;

const validEvent = {
  body: '{"test": "test"}',
};

const { validUser } = TestHelper;

describe('handler update method test suit', () => {
  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when passing a valid event and action to handler update method', () => {
    it('should return user data', async () => {
      const { data: user } = await createUser(validUser.user);

      const { uuid } = user;

      const userParams = {
        user: {
            contracts: ["16b50910-5cad-5f41-a46d-f1a8462f2b00", "ea180c6b-41be-50ee-ba14-4cf34b5f1be5"],
        },
      };

      const event = {
        pathParameters: { uuid: uuid },
        body: JSON.stringify(userParams),
      };

      const { body, errors } = await update(event);

      const { data } = JSON.parse(body);

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();
    });
  });

  describe.each([
    [
      null,
      BAD_REQUEST,
      buildRequiredErrorMessage('event', null, 'handler', 'handleAction'),
      'The request has failed, please try again',
    ],
    [
      undefined,
      BAD_REQUEST,
      buildRequiredErrorMessage('event', undefined, 'handler', 'handleAction'),
      'The request has failed, please try again',
    ],
  ])(
    'when passing invalid event as %s and action as %s to handleAction method',
    (event, expectedStatusCode, errorMessage, errorTitle) => {
      it('should return an error', async () => {
        const { statusCode, body } = await update(event);
        const { errors } = JSON.parse(body);

        expect(statusCode).toBe(expectedStatusCode);
        expect(errors).toBeTruthy();
        expect(errors.validation[0].details).toBe(errorMessage);
        expect(errors.title).toBe(errorTitle);
      });
    }
  );

  describe.each([
    [
      'uuid',
      null,
      BAD_REQUEST,
      buildRequiredErrorMessage('uuid', null, 'api', 'update'),
      'The request has failed, please try again',
    ],
    [
      'uuid',
      undefined,
      BAD_REQUEST,
      buildRequiredErrorMessage('uuid', undefined, 'api', 'update'),
      'The request has failed, please try again',
    ],
    [
      'uuid',
      '',
      BAD_REQUEST,
      buildRequiredErrorMessage('uuid', '', 'api', 'update'),
      'The request has failed, please try again',
    ]
  ])(
    'when passing invalid %s as %s to handleAction method',
    (fieldName, fieldValue, expectedStatusCode, errorMessage, errorTitle) => {
      it('should return an error', async () => {
        const pathParameters = {
          uuid: '123'
        };

        pathParameters[fieldName] = fieldValue;

        const { statusCode, body } = await update({ pathParameters, body: JSON.stringify(validUser) });
        const { errors } = JSON.parse(body);

        expect(statusCode).toBe(expectedStatusCode);
        expect(errors).toBeTruthy();
        expect(errors.validation[0].details).toBe(errorMessage);
        expect(errors.title).toBe(errorTitle);
      });
    }
  );

  describe.each([
    [{ 'Accept-Language': null }, 'en'],
    [{ 'Accept-Language': undefined }, 'en'],
    [{ 'Accept-Language': 'pt' }, 'pt'],
    [{ 'Accept-Language': 'en' }, 'en'],
  ])(
    'when passing accept language header as %j to handleAction method',
    (languageHeader, expectedLanguage) => {
      it(`should return language as ${expectedLanguage}`, async () => {
        const newEvent = { ...validEvent };
        newEvent.headers = { ...languageHeader };

        await update(newEvent);

        const newI18n = i18n;
        const newI18nLanguage = newI18n.getLocale();

        expect(newI18nLanguage).toBe(expectedLanguage);
      });
    }
  );

  describe.each([
    [{ test: 'test' }, BAD_REQUEST, 'The request has failed, please try again'],
    [null, BAD_REQUEST, 'The request has failed, please try again'],
    [undefined, BAD_REQUEST, 'The request has failed, please try again'],
    [{}, BAD_REQUEST, 'The request has failed, please try again'],
    ['', BAD_REQUEST, 'The request has failed, please try again'],
  ])(
    'when passing event body as %j to handleAction method',
    (pathParameters, expectedStatusCode, errorTitle) => {
      it(`should return an error`, async () => {
        const { statusCode, body } = await update({ pathParameters });
        const { errors } = JSON.parse(body);

        expect(statusCode).toBe(expectedStatusCode);
        expect(errors).toBeTruthy();
        expect(errors.title).toBe(errorTitle);
      });
    }
  );
});
