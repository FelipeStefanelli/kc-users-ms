const { list } = require('../handler');
const { createUser } = require('../src/database/create');
const { i18n } = require('../src/utils/i18n');
const Response = require('../src/utils/response');
const TestHelper = require('../config/tests/helper');
const { buildRequiredErrorMessage } = require('../src/database/__test__/helpers/messages.helper');

const { BAD_REQUEST } = Response.httpStatusCodes;

const validEvent = {
  body: '{"test": "test"}',
};

const createdUsers = [];

const addUser = async (contract) => {
  const { user } = TestHelper.validUser;
  const newUser = { ...user };
  newUser.contracts = contract;
  const { data } = await createUser(newUser);
  createdUsers.push(data);
};

describe('handler list method test suit', () => {
  beforeAll(async () => {
    await Promise.all([
        addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
        addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
        addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
        addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']),
        addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00'])
    ]);
  });

  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when passing a valid event and action to handler list method', () => {
    it('should return user data', async () => {
      const queryStringParameters = {
        'type@EQ': 'user',
      };

      const event = { queryStringParameters };
      const { body, errors } = await list(event);

      const { data } = JSON.parse(body);

      const { users } = data;

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();
      expect(users).toHaveLength(5);
      expect(users[0].contract).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[1].contract).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[2].contract).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[3].contract).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[4].contract).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
    });
  });

  describe.each([
    [
      null,
      BAD_REQUEST,
      buildRequiredErrorMessage('event', null, 'handler', 'handleAction'),
      'The request has failed, please try again',
    ],
    [
      undefined,
      BAD_REQUEST,
      buildRequiredErrorMessage('event', undefined, 'handler', 'handleAction'),
      'The request has failed, please try again',
    ],
  ])(
    'when passing invalid event as %s and action as %s to handleAction method',
    (event, expectedStatusCode, errorMessage, errorTitle) => {
      it('should return an error', async () => {
        const { statusCode, body } = await list(event);
        const { errors } = JSON.parse(body);

        expect(statusCode).toBe(expectedStatusCode);
        expect(errors).toBeTruthy();
        expect(errors.validation[0].details).toBe(errorMessage);
        expect(errors.title).toBe(errorTitle);
      });
    }
  );

  describe.each([
    [{ 'Accept-Language': null }, 'en'],
    [{ 'Accept-Language': undefined }, 'en'],
    [{ 'Accept-Language': 'pt' }, 'pt'],
    [{ 'Accept-Language': 'en' }, 'en'],
  ])(
    'when passing accept language header as %j to handleAction method',
    (languageHeader, expectedLanguage) => {
      it(`should return language as ${expectedLanguage}`, async () => {
        const newEvent = { ...validEvent };
        newEvent.headers = { ...languageHeader };

        await list(newEvent);

        const newI18n = i18n;
        const newI18nLanguage = newI18n.getLocale();

        expect(newI18nLanguage).toBe(expectedLanguage);
      });
    }
  );

  describe.each([
    [null, BAD_REQUEST, 'It was not possible to list the users'],
    [undefined, BAD_REQUEST, 'It was not possible to list the users'],
    ['', BAD_REQUEST, 'It was not possible to list the users'],
  ])(
    'when passing event queryStringParameters as %j to handleAction method',
    (queryStringParameters, expectedStatusCode, errorTitle) => {
      it(`should return an error`, async () => {
        const { statusCode, body } = await list({ queryStringParameters });
        const { errors } = JSON.parse(body);

        expect(statusCode).toBe(expectedStatusCode);
        expect(errors).toBeTruthy();
        expect(errors.title).toBe(errorTitle);
      });
    }
  );
});
