const { create } = require('../handler');
const { i18n } = require('../src/utils/i18n');
const Response = require('../src/utils/response');
const TestHelper = require('../config/tests/helper');
const { buildRequiredErrorMessage } = require('../src/database/__test__/helpers/messages.helper');

const {
  emptyType,
  requiredType,
  emptyName,
  requiredName,
  emptyBirthDate,
  requiredBirthDate,
  emptyIdentifier,
  requiredIdentifier,
  emptyIdentifierType,
  requiredIdentifierType,
  emptyPassword,
  requiredPassword,
  emptyContracts,
  requiredContracts,
  emptyAdress,
  requiredAdress,
  requiredAdressVerified,
  emptyAdressVerificationPhoto,
  requiredAdressVerificationPhoto,
  emptyPhone,
  requiredPhone,
  requiredPhoneVerified,
  emptyEmail,
  requiredEmail,
  requiredEmailVerified,
  requiredFirstLogin,
  requiredWelcomeEmailSent,
  emptyNotifications,
  requiredNotifications,
  emptyPhoto,
  requiredPhoto,
  emptyDriverLicenseNumber,
  requiredDriverLicenseNumber,
  emptyDriverLicenseRegisterNumber,
  requiredDriverLicenseRegisterNumber,
  emptyDriverLicenseFirstHabilitationAt,
  requiredDriverLicenseFirstHabilitationAt,
  emptyDriverLicenseExpiresAt,
  requiredDriverLicenseExpiresAt,
  emptyDriverLicenseCategory,
  requiredDriverLicenseCategory,
  emptyDriveLicensePhoto,
  requiredDriverLicensePhoto,
  requiredBlocked,
  emptyBlockedBy,
  requiredBlockedBy,
  emptyBlockedReason,
  requiredBlockedReason,
} = TestHelper.validationErrors;

const { BAD_REQUEST } = Response.httpStatusCodes;

const validEvent = {
  body: '{"test": "test"}',
};

const { validUser } = TestHelper;

describe('handler create method test suit', () => {
  afterAll(async () => {
    await TestHelper.clearTables();
  });

  describe('when passing a valid event and action to handler create method', () => {
    it('should return user data', async () => {
      const event = { body: JSON.stringify(validUser) };
      const { body, errors } = await create(event);

      const { data } = JSON.parse(body);

      expect(errors).toBeFalsy();
      expect(data).toBeTruthy();
      expect(data).toMatchObject(validUser.user);
    });
  });

  describe.each([
    [{ type: '' }, BAD_REQUEST, emptyType],
    [{ type: null }, BAD_REQUEST, requiredType],
    [{ type: undefined }, BAD_REQUEST, requiredType],
    [{ name: '' }, BAD_REQUEST, emptyName],
    [{ name: null }, BAD_REQUEST, requiredName],
    [{ name: undefined }, BAD_REQUEST, requiredName],
    [{ birth_date: '' }, BAD_REQUEST, emptyBirthDate],
    [{ birth_date: null }, BAD_REQUEST, requiredBirthDate],
    [{ birth_date: undefined }, BAD_REQUEST, requiredBirthDate],
    [{ identifier: '' }, BAD_REQUEST, emptyIdentifier],
    [{ identifier: null }, BAD_REQUEST, requiredIdentifier],
    [{ identifier: undefined }, BAD_REQUEST, requiredIdentifier],
    [{ identifier_type: '' }, BAD_REQUEST, emptyIdentifierType],
    [{ identifier_type: null }, BAD_REQUEST, requiredIdentifierType],
    [{ identifier_type: undefined }, BAD_REQUEST, requiredIdentifierType],
    [{ password: '' }, BAD_REQUEST, emptyPassword],
    [{ password: null }, BAD_REQUEST, requiredPassword],
    [{ password: undefined }, BAD_REQUEST, requiredPassword]
    [{ contracts: null }, BAD_REQUEST, requiredContracts],
    [{ contracts: undefined }, BAD_REQUEST, requiredContracts],
    [{ contracts: [] }, BAD_REQUEST, emptyContracts],
    [{ adress: '' }, BAD_REQUEST, emptyAdress],
    [{ adress: null }, BAD_REQUEST, requiredAdress],
    [{ adress: undefined }, BAD_REQUEST, requiredAdress],
    [{ adress_verified: '' }, BAD_REQUEST, requiredAdressVerified],
    [{ address_verification_photo: null }, BAD_REQUEST, requiredAdressVerificationPhoto],
    [{ address_verification_photo: undefined }, BAD_REQUEST, requiredAdressVerificationPhoto],
    [{ address_verification_photo: '' }, BAD_REQUEST, emptyAdressVerificationPhoto],
    [{ phone: '' }, BAD_REQUEST, emptyPhone],
    [{ phone: null }, BAD_REQUEST, requiredPhone],
    [{ phone: undefined }, BAD_REQUEST, requiredPhone],
    [{ phone_verified: '' }, BAD_REQUEST, requiredPhoneVerified],
    [{ email: '' }, BAD_REQUEST, emptyEmail],
    [{ email: null }, BAD_REQUEST, requiredEmail],
    [{ email: undefined }, BAD_REQUEST, requiredEmail],
    [{ email_verified: '' }, BAD_REQUEST, requiredEmailVerified],
    [{ first_login: '' }, BAD_REQUEST, requiredFirstLogin],
    [{ first_login: null }, BAD_REQUEST, requiredFirstLogin],
    [{ first_login: undefined }, BAD_REQUEST, requiredFirstLogin],
    [{ welcome_email_sent: '' }, BAD_REQUEST, requiredWelcomeEmailSent],
    [{ welcome_email_sent: null }, BAD_REQUEST, requiredWelcomeEmailSent],
    [{ welcome_email_sent: undefined }, BAD_REQUEST, requiredWelcomeEmailSent],
    [{ notifications: null }, BAD_REQUEST, requiredNotifications],
    [{ notifications: undefined }, BAD_REQUEST, requiredNotifications],
    [{ notifications: [] }, BAD_REQUEST, emptyNotifications],
    [{ photo: '' }, BAD_REQUEST, emptyPhoto],
    [{ photo: null }, BAD_REQUEST, requiredPhoto],
    [{ photo: undefined }, BAD_REQUEST, requiredPhoto],
    [{ driver_license_number: '' }, BAD_REQUEST, emptyDriverLicenseNumber],
    [{ driver_license_number: null }, BAD_REQUEST, requiredDriverLicenseNumber],
    [{ driver_license_number: undefined }, BAD_REQUEST, requiredDriverLicenseNumber],
    [{ driver_license_register_number: '' }, BAD_REQUEST, emptyDriverLicenseRegisterNumber],
    [{ driver_license_register_number: null }, BAD_REQUEST, requiredDriverLicenseRegisterNumber],
    [{ driver_license_register_number: undefined }, BAD_REQUEST, requiredDriverLicenseRegisterNumber],
    [{ driver_license_first_habilitation_at: '' }, BAD_REQUEST, emptyDriverLicenseFirstHabilitationAt],
    [{ driver_license_first_habilitation_at: null }, BAD_REQUEST, requiredDriverLicenseFirstHabilitationAt],
    [{ driver_license_first_habilitation_at: undefined }, BAD_REQUEST, requiredDriverLicenseFirstHabilitationAt],
    [{ driver_license_expires_at: '' }, BAD_REQUEST, emptyDriverLicenseExpiresAt],
    [{ driver_license_expires_at: null }, BAD_REQUEST, requiredDriverLicenseExpiresAt],
    [{ driver_license_expires_at: undefined }, BAD_REQUEST, requiredDriverLicenseExpiresAt],
    [{ driver_license_category: '' }, BAD_REQUEST, emptyDriverLicenseCategory],
    [{ driver_license_category: null }, BAD_REQUEST, requiredDriverLicenseCategory],
    [{ driver_license_category: undefined }, BAD_REQUEST, requiredDriverLicenseCategory],
    [{ driver_license_photo: '' }, BAD_REQUEST, emptyDriveLicensePhoto],
    [{ driver_license_photo: null }, BAD_REQUEST, requiredDriverLicensePhoto],
    [{ driver_license_photo: undefined }, BAD_REQUEST, requiredDriverLicensePhoto],
    [{ blocked: '' }, BAD_REQUEST, requiredBlocked],
    [{ blocked: null }, BAD_REQUEST, requiredBlocked],
    [{ blocked: undefined }, BAD_REQUEST, requiredBlocked],
    [{ blocked_by: '' }, BAD_REQUEST, emptyBlockedBy],
    [{ blocked_by: null }, BAD_REQUEST, requiredBlockedBy],
    [{ blocked_by: undefined }, BAD_REQUEST, requiredBlockedBy],
    [{ block_reason: '' }, BAD_REQUEST, emptyBlockedReason],
    [{ block_reason: null }, BAD_REQUEST, requiredBlockedReason],
    [{ block_reason: undefined }, BAD_REQUEST, requiredBlockedReason],
  ])('when passing invalid user field %j to handleAction', (field, expectedStatus, errorObject) => {
    it('should return an error', async () => {
      let newUser = { ...validUser.user };
      newUser = { ...field };
      
      const eventBody = { user: newUser };
      const event = { body: JSON.stringify(eventBody) };

      const { body, statusCode } = await create(event);
      const { data, errors } = JSON.parse(body);

      expect(statusCode).toBe(400);
      expect(data).toBeFalsy();
      expect(errors).toBeTruthy();
    });
  });

  describe.each([
    [
      null,
      BAD_REQUEST,
      buildRequiredErrorMessage('event', null, 'handler', 'handleAction'),
      'The request has failed, please try again',
    ],
    [
      undefined,
      BAD_REQUEST,
      buildRequiredErrorMessage('event', undefined, 'handler', 'handleAction'),
      'The request has failed, please try again',
    ],
  ])(
    'when passing invalid event as %s and action as %s to handleAction method',
    (event, expectedStatus, errorMessage, errorTitle) => {
      it('should return an error', async () => {
        const { statusCode, body } = await create(event);
        const { errors } = JSON.parse(body);

        expect(statusCode).toBe(400);
        expect(errors).toBeTruthy();
        expect(errors.title).toBe(errorTitle);
      });
    }
  );

  describe.each([
    [{ 'Accept-Language': null }, 'en'],
    [{ 'Accept-Language': undefined }, 'en'],
    [{ 'Accept-Language': 'pt' }, 'pt'],
    [{ 'Accept-Language': 'en' }, 'en'],
  ])(
    'when passing accept language header as %j to handleAction method',
    (languageHeader, expectedLanguage) => {
      it(`should return language as ${expectedLanguage}`, async () => {
        const newEvent = { ...validEvent };
        newEvent.headers = { ...languageHeader };

        await create(newEvent);

        const newI18n = i18n;
        const newI18nLanguage = newI18n.getLocale();

        expect(newI18nLanguage).toBe(expectedLanguage);
      });
    }
  );

  describe.each([
    [{ test: 'test' }, BAD_REQUEST, 'The request has failed, please try again'],
    [{}, BAD_REQUEST, 'The request has failed, please try again'],
  ])('when passing event body as %j to handleAction method', (eventBody, errorTitle) => {
    it(`should return an error`, async () => {
      const { statusCode, body } = await create({ body: eventBody });
      const { errors } = JSON.parse(body);

      expect(statusCode).toBe(400);
      expect(errors).toBeTruthy();
      expect(errors.title).toBe(errorTitle);
    });
  });
});
