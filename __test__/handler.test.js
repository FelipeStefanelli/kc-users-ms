const { handleAction } = require('../handler');
const { create } = require('../src/api/create');
const { remove } = require('../src/api/remove');
const { update } = require('../src/api/update');
const { get } = require('../src/api/get');
const { list } = require('../src/api/list');

const { createUser } = require('../src/database/create');

const { buildRequiredErrorMessage } = require('../src/database/__test__/helpers/messages.helper');
const Response = require('../src/utils/response');

const { BAD_REQUEST } = Response.httpStatusCodes;
const TestHelper = require('../config/tests/helper');

const { validUser } = TestHelper;

const createdUsers = [];

const addUser = async (contract) => {
  const { user } = TestHelper.validUser;
  const newUser = { ...user };
  newUser.contracts = contract;
  const { data } = await createUser(newUser);
  createdUsers.push(data);
};

describe('handler method test suit', () => {
  afterEach(async () => {
    await TestHelper.clearTables();
  });

  describe('when passing create method to handler', () => {
    it('should return user data', async () => {
      const event = { body: JSON.stringify(validUser) };
      const { body, statusCode } = await handleAction(event, create);

      const { data } = JSON.parse(body);

      expect(statusCode).toBe(200);
      expect(data).toBeTruthy();
      expect(data).toMatchObject(validUser.user);
    });
  });

  describe('when passing remove method to handler', () => {
    it('should return user data', async () => {
      const event = { body: JSON.stringify(validUser) };
      const { body, statusCode } = await handleAction(event, create);

      const { data } = JSON.parse(body);

      expect(statusCode).toBe(200);
      expect(data).toBeTruthy();
      expect(data).toMatchObject(validUser.user);
    });
  });

  describe('when passing update method to handler', () => {
    it('should return user data', async () => {
      const { data: user } = await createUser(validUser.user);

      const { uuid } = user;

      const userParams = {
        user: {
          contracts: ["16b50910-5cad-5f41-a46d-f1a8462f2b00", "ea180c6b-41be-50ee-ba14-4cf34b5f1be5"],
        },
      };

      const event = {
        pathParameters: { uuid: uuid },
        body: JSON.stringify(userParams),
      };

      const { body, statusCode } = await handleAction(event, update);

      const { data } = JSON.parse(body);

      expect(statusCode).toBe(200);
      expect(data).toBeTruthy();
    });
  });

  describe('when passing get method to handler', () => {
    it('should return user data', async () => {
      const { data: user } = await createUser(validUser.user);

      const { uuid } = user;

      const event = { pathParameters: { uuid } };
      const { body, statusCode } = await handleAction(event, get);
      const { data } = JSON.parse(body);

      expect(statusCode).toBe(200);
      expect(data).toBeTruthy();
      expect(data).toMatchObject(validUser.user);
    });
  });

  describe('when passing list method to handler', () => {
    it('should return user data', async () => {
      await addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      await addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      await addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      await addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);
      await addUser(['16b50910-5cad-5f41-a46d-f1a8462f2b00']);

      const queryStringParameters = {
        'type@EQ': 'user',
      };

      const event = { queryStringParameters };
      const { body, statusCode } = await handleAction(event, list);

      const { data } = JSON.parse(body);

      const { users } = data;

      expect(statusCode).toBe(200);
      expect(data).toBeTruthy();
      expect(users).toHaveLength(5);
      expect(users[0].contracts).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[1].contracts).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[2].contracts).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[3].contracts).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
      expect(users[4].contracts).toBe('16b50910-5cad-5f41-a46d-f1a8462f2b00');
    });
  });

  describe.each([
    [null, BAD_REQUEST, buildRequiredErrorMessage('action', null, 'handler', 'handleAction')],
    [undefined, BAD_REQUEST, buildRequiredErrorMessage('action', undefined, 'handler', 'handleAction')],
    ['', BAD_REQUEST, buildRequiredErrorMessage('action', '', 'handler', 'handleAction')],
  ])('when passing action as %j to handleAction method', (actionValue, expectedStatusCode, errorMessage) => {
    it(`should return an error`, async () => {
      const event = { body: JSON.stringify(validUser) };

      const { statusCode, body } = await handleAction(event, actionValue);

      const { errors } = JSON.parse(body);

      expect(statusCode).toBe(expectedStatusCode);
      expect(errors.validation[0].details).toBe(errorMessage);
    });
  });
});
