# Serverless (AWS) - NodeJS - DynamoDB - REST API - Users Microservice

```
This is the API interface for the users microservice built with the Serverless Framework using AWS as the provider, NodeJS for the REST API and DynamoDB for the database.
```

## How To Run

### Install dependencies
For this you will need to have installed NodeJS V10. If you don't have it installed, go to <https://nodejs.org/en/> and install it. 

Once you have it installed, run `npm install` to install the necessary packages

```
npm install
```
Then, install the serverless framework CLI globally
```
npm install serverless -g
``` 
You will need to have AWS Credentials configured in order to use the serverless offline plugin and to deploy your functions 
For this, go to <https://serverless.com/framework/docs/providers/aws/guide/credentials/> and see how to set the necessary environment variables
```
export AWS_ACCESS_KEY_ID=`YOUR_ACCESS_KEY_ID`
export AWS_SECRET_ACCESS_KEY=`YOUR_SECRET_ACCESS_KEY`
```
Set the DYNAMO_ENDPOINT environment variable to use the serverless-dynamodb-local plugin
``` 
export DYNAMO_ENDPOINT="http://localhost:8000"
```
Run the serverless-dynamodb-local plugin installation
```
serverless dynamodb install
```
After the installation, run the serverless-dynamodb-local plugin
```
serverless dynamodb start
```
Open another terminal and run the serverless-offline plugin
```
serverless offline
```
Open one more terminal, install dynamodb-admin and then run it
```
npm install dynamodb-admin -g
dynamodb-admin
```
Finally, you should be able to open any api integration tool like Postman and test your endpoints.