const { DocumentClient } = require('aws-sdk/clients/dynamodb');

const { DYNAMODB_TABLE } = process.env;
const isLocal = process.env.NODE_ENV == null;
const isTest = process.env.JEST_WORKER_ID;

let config = {
  convertEmptyValues: true,
};

// if (isLocal) {
//   config = {
//     ...config,
//     endpoint: 'http://localhost:8000',
//   };
// }

if (isTest) {
  const accessKey = process.env.AWS_ACCESS_KEY_ID;
  const secretKey = process.env.AWS_SECRET_ACCESS_KEY;
  const region = process.env.AWS_DEFAULT_REGION;
  const endpoint = process.env.DYNAMODB_ENDPOINT;

  config = {
    ...config,
    accessKeyId: accessKey,
    secretAccessKey: secretKey,
    region,
    endpoint: `http://${endpoint}`,
  };
}

const dynamoDb = new DocumentClient(config);

module.exports.dynamoDb = dynamoDb;
module.exports.tableName = DYNAMODB_TABLE;
