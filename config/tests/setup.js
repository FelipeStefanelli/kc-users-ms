const myDynamoDb = require('./myDynamoDb');

const setup = async () => {
  await myDynamoDb.start();
  await myDynamoDb.createTables();
};

module.exports = setup;
