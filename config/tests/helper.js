const myDynamoDb = require('./myDynamoDb');
const { t } = require('../../src/utils/i18n');

const clearTables = async () => {
  await myDynamoDb.deleteTables();
  await myDynamoDb.createTables();
};

const deleteTables = async () => myDynamoDb.deleteTables();

const createTables = async () => myDynamoDb.createTables();

const validUser = {
  user:{
      type: "user",
      name: "Pipo55",
      birth_date: "1995-03-19",
      identifier: "1324569811",
      identifier_type: "cpf/passaporte",
      password: "2RfUadZ8vkTlZpwCKMxLuN",
      contracts: [
          "16b50910-5cad-5f41-a46d-f1a8462f2b00",
          "ea180c6b-41be-50ee-ba14-4cf34b5f1be5"
      ],
      address: [{
          first_line: "teste",
          complement: "teste",
          city: "teste",
          state: "teste",
          country: "teste",
          zip_code: "teste"
      }],
      address_verified: false,
      address_verification_photo: "url",
      phone: "+5599123456789",
      phone_verified: false,
      email: "eva@brewer.com.br",
      email_verified: true,
      first_login: false,
      welcome_email_sent: false, 
      notifications: [{
          reservations_alert: false,
          ride_alert: false
      }],
      photo: "url",
      driver_license_number: "1234567890",
      driver_license_register_number: "1234567890",
      driver_license_first_habilitation_at: "2020-12-30",
      driver_license_expires_at: "2020-12-30",
      driver_license_category: "A/B/C/D",
      driver_license_photo: "url",
      blocked: false,
      blocked_by: "3816fa62-77a4-5f81-9876-43e547e76ad7",
      block_reason: "establish wherever sat language jar stopped line husband his herd with college"
  }
};

const validationErrors = {
  emptyType: {
    type: 'stringEmpty',
    field: 'type',
    message: t('validation.fields.type.empty'),
  },
  requiredType: {
    type: 'required',
    field: 'type',
    message: t('validation.fields.type.required'),
  },
  emptyName: {
    type: 'stringEmpty',
    field: 'name',
    message: t('validation.fields.name.empty'),
  },
  requiredName: {
    type: 'required',
    field: 'name',
    message: t('validation.fields.name.required'),
  },
  emptyBirth_date: {
    type: 'stringEmpty',
    field: 'birth_date',
    message: t('validation.fields.birth_date.empty'),
  },
  requiredBirth_date: {
    type: 'required',
    field: 'birth_date',
    message: t('validation.fields.birth_date.required'),
  },
  emptyIdentifier: {
    type: 'stringEmpty',
    field: 'identifier',
    message: t('validation.fields.identifier.empty'),
  },
  requiredIdentifier: {
    type: 'required',
    field: 'identifier',
    message: t('validation.fields.identifier.required'),
  },
  emptyIdentifierType: {
    type: 'stringEmpty',
    field: 'identifier_type',
    message: t('validation.fields.identifier_type.empty'),
  },
  requiredIdentifierType: {
    type: 'required',
    field: 'identifier_type',
    message: t('validation.fields.identifier_type.required'),
  },
  emptyPassword: {
    type: 'stringEmpty',
    field: 'password',
    message: t('validation.fields.password.empty'),
  },
  requiredPassword: {
    type: 'required',
    field: 'password',
    message: t('validation.fields.password.required'),
  },
  emptyContracts: {
    type: 'arrayEmpty',
    field: 'contracts',
    message: t('validation.fields.contracts.empty'),
  },
  requiredContracts: {
    type: 'required',
    field: 'contracts',
    message: t('validation.fields.contracts.required'),
  },
  emptyAdress: {
    type: 'stringEmpty',
    field: 'adress',
    message: t('validation.fields.adress.empty'),
  },
  requiredAdress: {
    type: 'required',
    field: 'adress',
    message: t('validation.fields.adress.required'),
  },
  requiredAdressVerified: {
    type: 'required',
    field: 'adress_verified',
    message: t('validation.fields.adress_verified.required'),
  },
  emptyAdressVerificationPhoto: {
    type: 'stringEmpty',
    field: 'address_verification_photo',
    message: t('validation.fields.address_verification_photo.empty'),
  },
  requiredAdressVerificationPhoto: {
    type: 'required',
    field: 'address_verification_photo',
    message: t('validation.fields.address_verification_photo.required'),
  },
  emptyPhone: {
    type: 'stringEmpty',
    field: 'phone',
    message: t('validation.fields.phone.empty'),
  },
  requiredPhone: {
    type: 'required',
    field: 'phone',
    message: t('validation.fields.phone.required'),
  },
  requiredPhoneVerified: {
    type: 'required',
    field: 'phone_verified',
    message: t('validation.fields.phone_verified.required'),
  },
  emptyEmail: {
    type: 'stringEmpty',
    field: 'email',
    message: t('validation.fields.email.empty'),
  },
  requiredEmail: {
    type: 'required',
    field: 'email',
    message: t('validation.fields.email.required'),
  },
  requiredEmailVerified: {
    type: 'required',
    field: 'email_verified',
    message: t('validation.fields.email_verified.required'),
  },
  requiredFirstLogin: {
    type: 'required',
    field: 'first_login',
    message: t('validation.fields.first_login.required'),
  },
  requiredWelcomeEmailSent: {
    type: 'required',
    field: 'welcome_email_sent',
    message: t('validation.fields.welcome_email_sent.required'),
  },
  emptyNotifications: {
    type: 'arrayEmpty',
    field: 'notifications',
    message: t('validation.fields.notifications.empty'),
  },
  requiredNotifications: {
    type: 'required',
    field: 'notifications',
    message: t('validation.fields.notifications.required'),
  },
  emptyPhoto: {
    type: 'stringEmpty',
    field: 'photo',
    message: t('validation.fields.photo.empty'),
  },
  requiredPhoto: {
    type: 'required',
    field: 'photo',
    message: t('validation.fields.photo.required'),
  },
  emptyDriverLicenseNumber: {
    type: 'stringEmpty',
    field: 'driver_license_number',
    message: t('validation.fields.driver_license_number.empty'),
  },
  requiredDriverLicenseNumber: {
    type: 'required',
    field: 'driver_license_number',
    message: t('validation.fields.driver_license_number.required'),
  },
  emptyDriverLicenseRegisterNumber: {
    type: 'stringEmpty',
    field: 'driver_license_register_number',
    message: t('validation.fields.driver_license_register_number.empty'),
  },
  requiredDriverLicenseRegisterNumber: {
    type: 'required',
    field: 'driver_license_register_number',
    message: t('validation.fields.driver_license_register_number.required'),
  },
  emptyDriverLicenseFirstHabilitationAt: {
    type: 'stringEmpty',
    field: 'driver_license_first_habilitation_at',
    message: t('validation.fields.driver_license_first_habilitation_at.empty'),
  },
  requiredDriverLicenseFirstHabilitationAt: {
    type: 'required',
    field: 'driver_license_first_habilitation_at',
    message: t('validation.fields.driver_license_first_habilitation_at.required'),
  },
  emptyDriverLicenseExpiresAt: {
    type: 'stringEmpty',
    field: 'driver_license_expires_at',
    message: t('validation.fields.driver_license_expires_at.empty'),
  },
  requiredDriverLicenseExpiresAt: {
    type: 'required',
    field: 'driver_license_expires_at',
    message: t('validation.fields.driver_license_expires_at.required'),
  },
  emptyDriverLicenseCategory: {
    type: 'stringEmpty',
    field: 'driver_license_category',
    message: t('validation.fields.driver_license_category.empty'),
  },
  requiredDriverLicenseCategory: {
    type: 'required',
    field: 'driver_license_category',
    message: t('validation.fields.driver_license_category.required'),
  },
  emptyDriveLicensePhoto: {
    type: 'stringEmpty',
    field: 'driver_license_photo',
    message: t('validation.fields.driver_license_photo.empty'),
  },
  requiredDriverLicensePhoto: {
    type: 'required',
    field: 'driver_license_photo',
    message: t('validation.fields.driver_license_photo.required'),
  },
  requiredBlocked: {
    type: 'required',
    field: 'blocked',
    message: t('validation.fields.blocked.required'),
  },
  emptyBlockedBy: {
    type: 'stringEmpty',
    field: 'blocked_by',
    message: t('validation.fields.blocked_by.empty'),
  },
  requiredBlockedBy: {
    type: 'required',
    field: 'blocked_by',
    message: t('validation.fields.blocked_by.required'),
  },
  emptyBlockedReason: {
    type: 'stringEmpty',
    field: 'block_reason',
    message: t('validation.fields.block_reason.empty'),
  },
  requiredBlockedReason: {
    type: 'required',
    field: 'block_reason',
    message: t('validation.fields.block_reason.required'),
  }
};

module.exports = {
  myDynamoDb,
  clearTables,
  validUser,
  validationErrors,
  deleteTables,
  createTables,
};
