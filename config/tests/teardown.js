const myDynamoDb = require('./myDynamoDb');

const teardown = async () => {
  await myDynamoDb.deleteTables();
  await myDynamoDb.finish();
};

module.exports = teardown;
