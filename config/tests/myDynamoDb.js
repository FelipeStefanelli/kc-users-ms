/* eslint-disable class-methods-use-this */
const DynamoDB = require('aws-sdk/clients/dynamodb');
const DynamoDbLocal = require('dynamodb-local');
const treeKill = require('tree-kill');

process.env.AWS_ACCESS_KEY_ID = 'access-key';
process.env.AWS_SECRET_ACCESS_KEY = 'secret-key';
process.env.AWS_DEFAULT_REGION = 'local-env';

process.env.DYNAMODB_TABLE = 'kc-users-ms-dev';
process.env.DYNAMODB_PORT = 8000;
process.env.DYNAMODB_ENDPOINT = `127.0.0.1:${process.env.DYNAMODB_PORT}`;

const DEFAULT_TABLE = process.env.DYNAMODB_TABLE;
const DEFAULT_PORT = process.env.DYNAMODB_PORT;
const DEFAULT_ENDPOINT = process.env.DYNAMODB_ENDPOINT;

const defaultConfig = {
  tables: [
    {
      TableName: DEFAULT_TABLE,
      KeySchema: [
        { AttributeName: 'uuid', KeyType: 'HASH' },
      ],
      AttributeDefinitions: [
        { AttributeName: 'uuid', AttributeType: 'S' },
      ],
      ProvisionedThroughput: { ReadCapacityUnits: 1, WriteCapacityUnits: 1 },
    },
  ],
  port: DEFAULT_PORT,
  endpoint: DEFAULT_ENDPOINT,
};

class MyDynamoDb {
  constructor(config = null) {
    if (!config) {
      this.config = defaultConfig;
    }
  }

  async start() {
    if (MyDynamoDb.server !== undefined && MyDynamoDb.server !== null) {
      return;
    }

    const { tables, port, endpoint } = this.config;
    const dynamoDbServer = await DynamoDbLocal.launch(port, null, ['-sharedDb']);

    const dynamodb = await new DynamoDB({
      endpoint,
      sslEnabled: false,
      region: process.env.AWS_DEFAULT_REGION,
    });

    MyDynamoDb.server = dynamoDbServer;
    MyDynamoDb.tables = tables;
    MyDynamoDb.client = dynamodb;
  }

  async finish() {
    if (MyDynamoDb.server === null || MyDynamoDb.server === undefined) {
      return;
    }

    treeKill(MyDynamoDb.server.pid, (err) => {
      if (err) {
        console.log(`Dynamodb process hasn't been killed : ${err}`);
      }
    });
  }

  async createTables() {
    return Promise.all(
      MyDynamoDb.tables.map((table) =>
        MyDynamoDb.client
          .createTable(table)
          .promise()
          .catch((err) => {
            console.error(err);
          })
      )
    );
  }

  async deleteTables() {
    if (MyDynamoDb.client === null || MyDynamoDb.client === undefined) {
      await this.start();
    }

    return Promise.all(
      MyDynamoDb.tables.map((table) =>
        MyDynamoDb.client
          .deleteTable({ TableName: table.TableName })
          .promise()
          .catch((err) => {
            console.error(err);
          })
      )
    );
  }
}

module.exports = new MyDynamoDb();
