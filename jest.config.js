module.exports = {
  globalSetup: './config/tests/setup.js',
  globalTeardown: './config/tests/teardown.js',
  testEnvironment: './config/tests/environment.js',
  preset: '@shelf/jest-dynamodb',
  collectCoverage: true,
  collectCoverageFrom: ['./src/**/*.{js,jsx}', './handler.js'],
  coveragePathIgnorePatterns: ['./src/lib'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: -10,
    },
  },
};
